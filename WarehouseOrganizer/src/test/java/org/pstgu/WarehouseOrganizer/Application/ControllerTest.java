package org.pstgu.WarehouseOrganizer.Application;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.pstgu.WarehouseOrganizer.InfoModel.RepositoryManagerImpl;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Task;

class ControllerTest {
	Controller controller = new Controller(new RepositoryManagerImpl("jdbc:sqlite:src/main/resources/db.sqlite"));

	@Test
	void executeTaskTest() {
		Task task1 = controller.getTasks().get(1);
		assertDoesNotThrow(() -> controller.execute(task1));
	}

	@Test
	void acceptOrderTest() {

		List<OrdProduct> productList3 = Arrays.asList(new OrdProduct(new Product("хлеб", "шт"), 10.0, "Заказ в пути"));

		assertDoesNotThrow(() -> controller.accept(new Order(LocalDate.parse("2021-02-10"),
				new Provider("TulsciiHleb", "456845852188", null, null), "Заказ в пути", productList3)));
	}
}
