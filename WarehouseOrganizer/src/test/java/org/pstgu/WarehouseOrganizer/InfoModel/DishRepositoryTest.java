package org.pstgu.WarehouseOrganizer.InfoModel;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;

class DishRepositoryTest {
	static RepositoryManagerImpl repManager;
	static DishRepository dishRepository;

	@BeforeAll
	static void initDBConnection() {
		repManager = new RepositoryManagerImpl("jdbc:sqlite:src/main/resources/db.sqlite");
		dishRepository = repManager.getDishRepository();
	}

	@Test
	void test() {
		boolean ok = true;
		if (ok)
			;
		else
			fail("la-la-la");
	}

	@Test
	void getAllTest() {
		int i = 1;
		for (Dish dish : dishRepository.getAll()) {
			System.out.println(i++ + " " + dish.toString());
		}
	}

	@Test
	void saveDishTest() throws NotFoundException {
		List<DishIngredient> ingredients = Arrays.asList(
				new DishIngredient(new Product("Гречневая крупа", "кг"), 2.5, 5.0, "Варить в соотношении 1 к 3"));
		Dish dish = new Dish("Гречка", "Гранир", ingredients, "Подавать горячим");

		assertEquals(true, dishRepository.add(dish));

		assertDoesNotThrow(new Executable() {

			@Override
			public void execute() throws Throwable {
				Dish actual = dishRepository.get(dish.getName());

			}
		});

		assertEquals(dish, dishRepository.get(dish.getName()));
	}

	@AfterAll
	static void closeDBConnection() {
		DishRepositoryTest.initDBConnection();

		repManager.closeDBConnection();
	}

}
