package my.java.sql;

import java.time.LocalDate;
import java.util.Arrays;

import org.pstgu.WarehouseOrganizer.InfoModel.OrderRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.RepositoryManagerImpl;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;

public class TestGeneratedKeys {

	private static RepositoryManagerImpl repositoryManager;

	public static void main(String[] args) {
		repositoryManager = new RepositoryManagerImpl("jdbc:sqlite:src/main/resources/db.sqlite");

		OrderRepository orderRepository = repositoryManager.getOrderRepository();

		orderRepository.add(new Order(LocalDate.now(), new Provider(), "",
				Arrays.asList(new OrdProduct(new Product("Хлеб черный", "шт"), 35.0, ""))));

		repositoryManager.closeDBConnection();
	}
}
