package com.example;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class HelloWorldOnJavaFX extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		Label hello = new Label("Hello!");

		BorderPane root = new BorderPane();
		root.setCenter(hello);

		Scene scene = new Scene(root, 400, 300);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
