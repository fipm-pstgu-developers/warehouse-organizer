/*package javafx.table;

public class Person {

}*/
/* ....Show License.... */
package javafx.table.TableCellFactoryApp;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Person {

        private final BooleanProperty invited;
        private final StringProperty firstName;
        private final StringProperty lastName;
        private final StringProperty email;

        public Person(boolean invited, String first, String last, String email) {
            this.invited = new SimpleBooleanProperty(invited);
            this.firstName = new SimpleStringProperty(first);
            this.lastName = new SimpleStringProperty(last);
            this.email = new SimpleStringProperty(email);
        }

        public BooleanProperty invitedProperty() {
            return invited;
        }
        public void setProperty(Boolean value) {
        	invited.set(value);
        }

        public StringProperty firstNameProperty() {
            return firstName;
        }
        public void setFirstName(String value) {
        	firstName.set(value);
        }

        public StringProperty lastNameProperty() {
            return lastName;
        }
        public void setLastName(String value) {
        	lastName.set(value);
        }

        public StringProperty emailProperty() {
            return email;
        }
        public void setEmail(String value) {
        	email.set(value);
        }
}