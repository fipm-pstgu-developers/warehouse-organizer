package javafx.layout;

import javafx.application.Application;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;

public class WorkWithBounds extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	private static void setSideAnchors(Node node) {
		AnchorPane.setLeftAnchor(node, 0d);
		AnchorPane.setRightAnchor(node, 0d);
	}

	@Override
	public void start(Stage primaryStage) {
		// create covering area
		Region topRegion = new Region();
		topRegion.setStyle("-fx-background-color: white;");
		Polygon topArrow = new Polygon(0, 0, 20, 0, 10, 20);
		topArrow.setFill(Color.WHITE);
		VBox top = new VBox(topRegion, topArrow);
		top.setAlignment(Pos.TOP_CENTER);
		topArrow.setOnMouseClicked(evt -> {
			topRegion.setPrefHeight(topRegion.getPrefHeight() + 10);
		});

		// create bottom covering area
		Region bottomRegion = new Region();
		bottomRegion.setStyle("-fx-background-color: white;");
		Polygon bottomArrow = new Polygon(0, 20, 20, 20, 10, 0);
		bottomArrow.setFill(Color.WHITE);
		VBox bottom = new VBox(bottomArrow, bottomRegion);
		bottom.setAlignment(Pos.BOTTOM_CENTER);
		bottomArrow.setOnMouseClicked(evt -> {
			bottomRegion.setPrefHeight(bottomRegion.getPrefHeight() + 10);
		});

		Image image = new Image(
				"https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg/402px-Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg");
		ImageView imageView = new ImageView(image);

		setSideAnchors(top);
		setSideAnchors(bottom);
		setSideAnchors(imageView);
		AnchorPane.setTopAnchor(top, 0d);
		AnchorPane.setBottomAnchor(bottom, 0d);
		AnchorPane.setTopAnchor(imageView, 0d);
		AnchorPane.setBottomAnchor(imageView, 0d);

		AnchorPane container = new AnchorPane(imageView, top, bottom);

		ImageView imageViewRestricted = new ImageView(image);

		Button button = new Button("restrict");
		button.setOnAction(evt -> {
			// determine bouns of Regions in AnchorPane
			Bounds topBounds = top.localToParent(topRegion.getBoundsInParent());
			Bounds bottomBounds = bottom.localToParent(bottomRegion.getBoundsInParent());

			// set viewport accordingly
			imageViewRestricted.setViewport(new Rectangle2D(0, topBounds.getMaxY(), image.getWidth(),
					bottomBounds.getMinY() - topBounds.getMaxY()));
		});

		HBox root = new HBox(container, button, imageViewRestricted);
		root.setFillHeight(false);

		Scene scene = new Scene(root);

		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
