--
-- Использованная кодировка текста: UTF-8
--

-- Таблица: Products
-- Продукты - это все возможные продукты, которые могут храниться на складе
CREATE TABLE IF NOT EXISTS Products (
	id INTEGER PRIMARY KEY, 
	name TEXT NOT NULL UNIQUE, 
	measureUnit TEXT NOT NULL
);

--INDEX 

-- Склад - все продукты, имеющиеся на складе с их количеством
CREATE TABLE IF NOT EXISTS FoodInStock (
	id INTEGER PRIMARY KEY,
	productId INTEGER,
	numberOf REAL, 
	location TEXT,
	FOREIGN KEY (productId) REFERENCES Products (id)
);

-- Заказы - это список заказов у посатавщиков продуктов.
CREATE TABLE IF NOT EXISTS Orders (
	id INTEGER PRIMARY KEY,
	creationDate TEXT NOT NULL,
	providerId INTEGER,
	sendingDate TEXT,
	receiveDate TEXT,
	comment TEXT,
	FOREIGN KEY (providerId) REFERENCES Provider (id)
);

-- Поставщики
CREATE TABLE IF NOT EXISTS Provider (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL,
	phone TEXT NOT NULL
);

-- Продукты, которые может привезти поставщик
CREATE TABLE IF NOT EXISTS SuppliedProducts (
	productId INTEGER,
	providerId INTEGER,
	price REAL,
	PRIMARY KEY (productId, providerId),
	FOREIGN KEY (productId) REFERENCES Products (id),
	FOREIGN KEY (providerId) REFERENCES Provider (id)
);

-- Дни, в которые приезжает поставщик
CREATE TABLE IF NOT EXISTS DeliveryDays (
	id INTEGER PRIMARY KEY, 
	providerId INTEGER, 
	dayOfWeek TEXT, 
	preferredTime TEXT, 
	FOREIGN KEY (providerId) REFERENCES Provider (id)
);


-- Наполнение заказа - таблица продуктов в заказе
CREATE TABLE IF NOT EXISTS OrderFill (
	orderId INTEGER,
	productId INTEGER,
	expectedNumberOf REAL,
	actualNumberOf REAL,
	comment TEXT,
	PRIMARY KEY (orderId, productId),
	FOREIGN KEY (orderId) REFERENCES Orders (id),
	FOREIGN KEY (productId) REFERENCES Products (id)
);

-- Ингредиенты - тип закупки
CREATE TABLE IF NOT EXISTS PurchaseType (
	id INTEGER PRIMARY KEY,
	productId INTEGER,
	type TEXT,
	FOREIGN KEY (productId) REFERENCES Products (id)
);

-- Тип: супы, гарниры, напитки
CREATE TABLE IF NOT EXISTS DishType (
	id INTEGER PRIMARY KEY,
	type TEXT NOT NULL UNIQUE
);

-- Блюдо из справочника блюд
CREATE TABLE IF NOT EXISTS Dish (
	id INTEGER PRIMARY KEY,
	dishName TEXT NOT NULL UNIQUE,
	dishTypeId INTEGER,
	comment TEXT,
	FOREIGN KEY (dishTypeId) REFERENCES DishType (id)
);

-- Ингредиент блюда
CREATE TABLE IF NOT EXISTS DishIngredient (
	id INTEGER PRIMARY KEY,
	dishId INTEGER,
	productId INTEGER,
	expectedNumberOf REAL,
	actualNumberOf REAL,
	cookingType TEXT,
	FOREIGN KEY (dishId) REFERENCES Dish (id),
	FOREIGN KEY (productId) REFERENCES Products (id)
);

-- Меню на день
CREATE TABLE IF NOT EXISTS Menu (
	id INTEGER PRIMARY KEY,
	date TEXT NOT NULL,
	type TEXT CHECK( type IN ('Завтрак','Обед','Ужин','Предотбойник') ),
	dishId INTEGER,
	comment TEXT,
	givenOut TEXT,
	FOREIGN KEY (dishId) REFERENCES Dish (id)
);
