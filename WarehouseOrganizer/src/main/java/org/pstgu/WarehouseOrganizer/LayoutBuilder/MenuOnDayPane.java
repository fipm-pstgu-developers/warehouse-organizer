package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

import org.pstgu.WarehouseOrganizer.InfoModel.NotFoundException;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Menu.FoodReception;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.MenuRepositoryBinder;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.BorderPane;

public class MenuOnDayPane extends BorderPane implements Initializable {
	@FXML
	private TreeTableColumn<DishIngredient, String> actualNumberOfCol;
	@FXML
	private Button backButton;
	@FXML
	private TreeTableColumn<DishIngredient, String> nameCol;
	@FXML
	private TreeTableColumn<DishIngredient, String> expectedNumberOfCol;
	@FXML
	private TreeTableColumn<DishIngredient, String> commentCol;
	@FXML
	private TreeTableColumn<DishIngredient, String> operationsCol;
	@FXML
	private ComboBox<String> ingredientComboBox;
	@FXML
	private Label titleLabel;
	@FXML
	private TreeTableView<DishIngredient> treeTableView;
	@FXML
	private ComboBox<String> typeComboBox;

	private EventHandler<ActionEvent> backButtonAction;
	private MenuRepositoryBinder model;
	private LocalDate date;

	public MenuOnDayPane(LocalDate day, MenuRepositoryBinder menuRepositoryBinder,
			EventHandler<ActionEvent> backButtonAction) {
		model = menuRepositoryBinder;
		this.backButtonAction = backButtonAction;
		date = day;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MenuOnDay.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		titleLabel.setText("МЕНЮ НА " + date.format(DateTimeFormatter.ofPattern("dd.MM")));

		prefTable();
	}

	private void prefTable() {
		try {
			Map<FoodReception, List<Dish>> menu = model.getMenuOnDay(date);
			TreeItem<DishIngredient> menuOnDay = new TreeItem<>(
					new DishIngredient(new Product("Меню на день", "null"), null, null, null)); // HACK
			menuOnDay.setExpanded(true);
			for (Map.Entry<FoodReception, List<Dish>> foodReception : new TreeMap<FoodReception, List<Dish>>(menu)
					.entrySet()) {
				TreeItem<DishIngredient> root = new TreeItem<>(
						new DishIngredient(new Product(foodReception.getKey().name, "null"), null, null, null)); // HACK
				menuOnDay.getChildren().add(root);
				for (Dish dish : foodReception.getValue()) {
					TreeItem<DishIngredient> dishName = new TreeItem<>(
							new DishIngredient(new Product(dish.getName(), "null"), null, null, null)); // HACK
					root.getChildren().add(dishName);
					for (DishIngredient ingredient : dish.getIngredients()) {
						TreeItem<DishIngredient> ingr = new TreeItem<>(ingredient);
						dishName.getChildren().add(ingr);
					}
				}
			}
			nameCol.setCellValueFactory(
					param -> new ReadOnlyStringWrapper(param.getValue().getValue().getProduct().getName()));
			expectedNumberOfCol.setCellValueFactory(param -> new ReadOnlyStringWrapper(
					param.getValue().getValue().getExpectedNumberOf() != null
							? param.getValue().getValue().getExpectedNumberOf().toString() + " "
									+ param.getValue().getValue().getProduct().getMeasureUnit()
							: ""));
			actualNumberOfCol.setCellValueFactory(param -> new ReadOnlyStringWrapper(
					param.getValue().getValue().getActualNumberOf() != null
							? param.getValue().getValue().getActualNumberOf().toString() + " "
									+ param.getValue().getValue().getProduct().getMeasureUnit()
							: ""));
			commentCol
					.setCellValueFactory(param -> new ReadOnlyStringWrapper(param.getValue().getValue().getComment()));

			treeTableView.setRoot(menuOnDay);
			treeTableView.setShowRoot(true);
		} catch (NotFoundException e) {
			e.printStackTrace();
		}

//		final TreeTableColumn<Menu, String> nameColumn =
//	            new TreeTableColumn<>("Name");
//	        nameColumn.setEditable(false);
//	        nameColumn.setMinWidth(130);
//	        nameColumn.setCellValueFactory(new TreeItemPropertyValueFactory("name"));
//
//			TableColumnFactory.prefStringColumn(nameColumn, (Order o) -> model.providerNameProperty(o));
//
//
//
//	        final TreeTableColumn<Menu, String> dataColumn =
//	            new TreeTableColumn<>("Data");
//	        dataColumn.setEditable(false);
//	        dataColumn.setMinWidth(150);
//	        dataColumn.setCellValueFactory(new TreeItemPropertyValueFactory("data"));
//
//	        final TreeTableColumn<Menu, String> notesColumn =
//	            new TreeTableColumn<>("Notes (editable)");
//	        final Callback<TreeTableColumn<Menu,String>,
//	                                       TreeTableCell<Menu,String>> notes =
//	            TextFieldTreeTableCell.<Menu>forTreeTableColumn();
//	        notesColumn.setEditable(true);
//	        notesColumn.setMinWidth(150);
//	        notesColumn.setCellValueFactory(new TreeItemPropertyValueFactory("notes"));
//	        notesColumn.setCellFactory(notes);
//
//	        final TreeTableView treeTableView = new TreeTableView(getData());
//	        treeTableView.setEditable(true);
//	        treeTableView.setPrefSize(430, 200);
//	        treeTableView.getColumns().setAll(nameColumn, dataColumn, notesColumn);
//
	}

//	private TreeItem<Menu> getTableData() {
//        final TreeItem<Menu> rootItem = new TreeItem<>(
//                new Menu(LocalDate.parse("2021-03-04"),  	));
//
//
//        final TreeItem<Menu> child1Item = new TreeItem<>(
//                new Menu("Child 1",
//                              new Data("Child 1 data"), "My notes"));
//        final TreeItem<Menu> child2Item = new TreeItem<>(
//                new Menu("Child 2",
//                              new Data("Child 2 data"), "Notes"));
//        TreeItem<Menu> child3Item = new TreeItem<>(
//                new Menu("Child 3",
//                              new Data("Child 3 data"), "Observations"));
//        rootItem.setExpanded(true);
//        rootItem.getChildren().addAll(child1Item, child2Item);
//        child1Item.getChildren().add(child3Item);
//        return rootItem;
//    }
}
