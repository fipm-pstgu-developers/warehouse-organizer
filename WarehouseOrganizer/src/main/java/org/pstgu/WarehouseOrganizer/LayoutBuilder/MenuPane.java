package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.pstgu.WarehouseOrganizer.InfoModel.MenuRepository;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.MenuRepositoryBinder;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class MenuPane extends BorderPane implements Initializable {

	@FXML
	private Button backButton;
	@FXML
	private Label dateLbl;
	@FXML
	private Button todayBtn;
	@FXML
	private Button tomorrowBtn;
	@FXML
	private BorderPane calendarPane;

	private DatePicker datePicker = new DatePicker(LocalDate.now());

	private EventHandler<ActionEvent> backButtonAction;
	private MenuRepositoryBinder model;
	private Function<Node, Void> permissionToShow;

	public MenuPane(MenuRepository menuRepository, EventHandler<ActionEvent> backButtonAction) {
		model = new MenuRepositoryBinder(menuRepository);
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MenuForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		calendarPane.setCenter(datePicker);

		Function<LocalDate, Void> openMenu = new Function<LocalDate, Void>() {
			@Override
			public Void apply(LocalDate date) {
				if (date != null) {
					MenuOnDayPane menuOnDayPane = new MenuOnDayPane(date, model, backButtonAction);
					if (permissionToShow != null) {
						permissionToShow.apply(menuOnDayPane);
					}
				}
				return null;
			}
		};

		todayBtn.setOnAction(e -> openMenu.apply(LocalDate.now()));
		tomorrowBtn.setOnAction(e -> openMenu.apply(LocalDate.now().plusDays(1)));
		datePicker.valueProperty().addListener((o, oV, nV) -> openMenu.apply(nV));
	}

	void givePermission(Function<Node, Void> toShow) {
		permissionToShow = toShow;
	}

}
