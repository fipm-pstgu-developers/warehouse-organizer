package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.DishRepositoryBinder;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

public class DishViewPane extends BorderPane implements Initializable {
	@FXML
	private Button backButton;
	@FXML
	private Label ingredientName;
	@FXML
	private TableColumn<DishIngredient, String> nameCol;
	@FXML
	private TableColumn<DishIngredient, String> numberOfCol;
	@FXML
	private TableView<DishIngredient> tableView;

	private EventHandler<ActionEvent> backButtonAction;
	private DishRepositoryBinder model;
	private Dish dish;

	public DishViewPane(Dish dish, DishRepositoryBinder cookbookRepositoryController,
			EventHandler<ActionEvent> backButtonAction) {
		model = cookbookRepositoryController;
		this.backButtonAction = backButtonAction;
		this.dish = dish;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("DishViewForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		ingredientName.setText(dish.getName());

		prefTable();

	}

	private void prefTable() {

		TableColumnFactory.prefStringColumn(nameCol, (DishIngredient d) -> model.ingredientNameProperty(d));
		TableColumnFactory.prefStringColumn(numberOfCol, (DishIngredient d) -> model.ingredientNumberOfProperty(d));
//
//		TableColumnFactory.prefStringColumn(ingredientsCol, (Dish d) -> model.getDishIngredientsProperty(d));
//
		tableView.setItems(model.getDishIngredientsList(dish));
	}

}