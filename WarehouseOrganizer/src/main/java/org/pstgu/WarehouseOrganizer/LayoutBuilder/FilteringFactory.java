package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;

public class FilteringFactory {

	private FilteringFactory() {
	}

	public static <T> ObservableList<T> wrapInFilteredList(
			Map<? extends ObservableValue<String>, List<BiPredicate<T, String>>> filters, ObservableList<T> masterData,
			ReadOnlyObjectProperty<Comparator<T>> comparatorForSoting) {

		Map<? extends ObservableValue<String>, BiPredicate<T, String>> compactFilters = filters.entrySet()
				.parallelStream().collect(Collectors.toMap(e -> e.getKey(),
						e -> e.getValue().stream().reduce(BiPredicate::or).orElse((x, y) -> true)));

		// 1. Wrap the ObservableList in a FilteredList (initially display all data).
		FilteredList<T> filteredData = new FilteredList<>(masterData, p -> true);

		filteredData.predicateProperty().bind(Bindings.createObjectBinding(() -> {
//			System.err.println("filteredData.size() = " + filteredData.size());
			return (element) -> {
				// Apply all filters
				return compactFilters.entrySet().parallelStream().map(keyAndValue -> {
					String newValue = keyAndValue.getKey().getValue();
					// If filter text is empty, display all.
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}
					return keyAndValue.getValue().test(element, newValue.toLowerCase());
				})
				.allMatch((e) -> e == true);
			};
		}, filters.keySet().toArray(new Observable[0])));

		// 3. Wrap the FilteredList in a SortedList.
		SortedList<T> sortedData = new SortedList<>(filteredData);

		// 4. Bind the SortedList comparator to the TableView comparator.
		// Otherwise, sorting the TableView would have no effect.
		sortedData.comparatorProperty().bind(comparatorForSoting);

		// 5. Return sorted (and filtered) data.

		return sortedData;
	}
}
