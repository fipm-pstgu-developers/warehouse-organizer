package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.pstgu.WarehouseOrganizer.InfoModel.DishRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.OrderRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.OrderRepositoryBinder;



import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

public class DeliverySchedulePane extends BorderPane implements Initializable {
	@FXML
    private TableView<Provider> DeliveryShTable;

    @FXML
    private TableColumn<Provider, String> deliveryShTableColDays;

    @FXML
    private TableColumn<Provider, String> deliveryShTableColOperations;

    @FXML
    private TableColumn<Provider, String> deliveryShTableColPhone;

    @FXML
    private TableColumn<Provider, String> deliveryShTableProvider;
	@FXML
	private Button backButton;

	private OrderRepositoryBinder model;
	private EventHandler<ActionEvent> backButtonAction;

	public DeliverySchedulePane(OrderRepository orderRepository, EventHandler<ActionEvent> backButtonAction) {
		model = new OrderRepositoryBinder(orderRepository, null);
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("DeliveryScheduleForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);
		prefTable();
	}
	private void prefTable() {
		
		TableColumnFactory.prefStringColumn(deliveryShTableProvider, (Provider d) -> model.providerNameProperty(d));
		TableColumnFactory.prefStringColumn(deliveryShTableColPhone, (Provider d) -> model.providerPhoneProperty(d));
		TableColumnFactory.prefStringColumn( deliveryShTableColDays, (Provider d) -> model.providerDaysProperty(d));
		
		DeliveryShTable.setItems(model.getProviders());
	}
}
