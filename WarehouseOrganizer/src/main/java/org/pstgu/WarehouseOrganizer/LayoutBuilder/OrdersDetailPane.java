package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Consumer;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.OrderRepositoryBinder;

import javafx.beans.InvalidationListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class OrdersDetailPane extends BorderPane implements Initializable {

	@FXML
	private Button backButton;
	@FXML
	private Label titleLbl;

	@FXML
	private VBox ordersDetailPane;

	private List<Order> orders;

	private OrderRepositoryBinder model;
	private EventHandler<ActionEvent> executeTaskAction;
	private EventHandler<ActionEvent> backButtonAction;

	public OrdersDetailPane(List<Order> orders, OrderRepositoryBinder modelBinder,
			EventHandler<ActionEvent> executeTaskAction, EventHandler<ActionEvent> backButtonAction) {
		this.orders = orders;
		this.model = modelBinder;
		this.executeTaskAction = executeTaskAction;
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("OrdersDetailFrame.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		if (orders != null)
			for (Order order : orders) {
				OrderDetailTemplate orderDetailTable = new OrderDetailTemplate(order, model);
				orderDetailTable.setMinHeight(400);
				// TODO resizing ordersDetailPane
				// ordersDetailPane.minHeightProperty().bind(orderDetailTable.heightProperty().add(ordersDetailPane.heightProperty()));
				// orderDetailTable.givePermission(p -> {
				// ordersDetailPane.getChildren().add(orderDetailTable);
				// return null;
				// });
				orderDetailTable.setToSendOrder(ord -> {
					ord.setSendingDate(LocalDate.now());
					model.updateOrder(ord);
					ordersDetailPane.getChildren().remove(orderDetailTable);
					return null;
				});
				ordersDetailPane.getChildren().add(orderDetailTable);
			}

		ordersDetailPane.getChildren().addListener((InvalidationListener) o -> {
			if (ordersDetailPane.getChildren().isEmpty()) {
				executeTaskAction.handle(null);
			}
		});

		titleLbl.setText("ЗАКАЗЫ " + LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM")));
	}
}
