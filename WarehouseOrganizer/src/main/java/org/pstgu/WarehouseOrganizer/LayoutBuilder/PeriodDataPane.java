package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.prefs.Preferences;

import org.pstgu.WarehouseOrganizer.Utils.PreferencesManager;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;

public class PeriodDataPane extends BorderPane implements Initializable {

	@FXML
	private Button backButton;

	@FXML
	private TextField numberOfChildrenLbl;

	@FXML
	private ToggleButton numberOfChildrenBtn;

	@FXML
	private TextField numberOfAdultsLbl;

	@FXML
	private ToggleButton numberOfAdultsBtn;

	private Function<Node, Void> permissionToShow;
	private EventHandler<ActionEvent> backButtonAction;

	private PreferencesManager prefs;

	public PeriodDataPane(EventHandler<ActionEvent> backButtonAction) {
		this.backButtonAction = backButtonAction;
		prefs = PreferencesManager.getInstance();

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PeriodDataForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		prefNumberOfBtn(numberOfChildrenBtn, numberOfChildrenLbl, PreferencesManager.NUM_OF_CHILDREN);
		prefNumberOfBtn(numberOfAdultsBtn, numberOfAdultsLbl, PreferencesManager.NUM_OF_ADULTS);

	}

	private void prefNumberOfBtn(ToggleButton numberOfBtn, TextField numberOfLbl, String NUMBER_OF_KEY) {
		numberOfLbl.setText(prefs.get(NUMBER_OF_KEY, PreferencesManager.DEFAULT_NUM_OF)+"");
		numberOfLbl.setEditable(false);

		numberOfBtn.setOnAction(event -> {
			if (numberOfBtn.isSelected()) {
				numberOfLbl.setEditable(true);
				numberOfBtn.setText("[СОХР]");
			} else {
				numberOfLbl.setEditable(false);
				numberOfBtn.setText("[РЕД]");
				prefs.put(NUMBER_OF_KEY, Integer.parseInt(numberOfLbl.getText()));
			}
		});
	}

	void givePermission(Function<Node, Void> toShow) {
		permissionToShow = toShow;
	}
}
