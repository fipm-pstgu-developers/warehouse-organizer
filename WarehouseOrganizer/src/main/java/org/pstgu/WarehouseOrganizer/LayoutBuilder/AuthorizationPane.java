package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class AuthorizationPane extends BorderPane implements Initializable {

	@FXML
	private Button loginbtn;

	@FXML
	private TextField logField;

	@FXML
	private PasswordField passField;

	private EventHandler<ActionEvent> backButtonAction;

	public AuthorizationPane(EventHandler<ActionEvent> backButtonAction) {
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AuthorizationForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loginbtn.setOnAction(backButtonAction);
	}

}
