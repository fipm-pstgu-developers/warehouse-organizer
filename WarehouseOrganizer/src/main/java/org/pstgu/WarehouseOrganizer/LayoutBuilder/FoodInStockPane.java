package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import org.pstgu.WarehouseOrganizer.InfoModel.FoodRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.FoodProduct;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.FoodRepositoryBinder;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class FoodInStockPane extends BorderPane implements Initializable {
	@FXML
	private Button backButton;
	@FXML
	private Button editButton;
	@FXML
	private TableColumn<FoodProduct, String> nameCol;
	@FXML
	private TableColumn<FoodProduct, String> numberOfCol;
	@FXML
	private TableColumn<FoodProduct, String> measureUnitCol;
	@FXML
	private TableColumn<FoodProduct, String> locationCol;
	@FXML
	private ComboBox<String> productsCatalogFilter;
	@FXML
	private TextField searchField;
	@FXML
	private TableView<FoodProduct> tableView;

	private EventHandler<ActionEvent> backButtonAction;
	private FoodRepositoryBinder model;

	public FoodInStockPane(FoodRepository foodRepository, EventHandler<ActionEvent> backButtonAction) {
		model = new FoodRepositoryBinder(foodRepository);
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FoodInStockForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		productsCatalogFilter.setItems(FXCollections
				.observableArrayList(model.getListOfFoodProduct().stream().filter(fProd -> fProd.getLocation() != null)
						.map(fProd -> fProd.getLocation()).distinct().collect(Collectors.toList())));
		productsCatalogFilter.getItems().add(0, "");

		TableColumnFactory.prefStringColumn(nameCol, (FoodProduct p) -> model.nameProperty(p));
		TableColumnFactory.prefStringColumn(numberOfCol, (FoodProduct p) -> model.numberOfProperty(p));
		TableColumnFactory.prefStringColumn(measureUnitCol, (FoodProduct p) -> model.measureUnitProperty(p));
		TableColumnFactory.prefStringColumn(locationCol, (FoodProduct p) -> model.locationProperty(p));

		loadTableData();

	}

	private void loadTableData() {
		Map<ObservableValue<String>, List<BiPredicate<FoodProduct, String>>> filters = new HashMap<>();

		filters.put(searchField.textProperty(),
				Arrays.asList(
						(productInStock,
								requestString) -> productInStock.getProduct().getName().toLowerCase()
										.indexOf(requestString) != -1,
						(productInStock, requestString) -> productInStock.getProduct().getMeasureUnit().toLowerCase()
								.indexOf(requestString) != -1));
		filters.put(productsCatalogFilter.valueProperty(),
				Arrays.asList((productInStock, requestString) -> productInStock.getLocation() != null
						? productInStock.getLocation().equals(requestString)
						: false));

		tableView.setItems(FilteringFactory.wrapInFilteredList(filters, model.getListOfFoodProduct(),
				tableView.comparatorProperty()));
	}

	public void update() {
		loadTableData();
		tableView.refresh();

	}
}
