package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Function;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public class SettingsPane extends BorderPane implements Initializable {
	

	@FXML
	private Button backButton;

	private EventHandler<ActionEvent> backButtonAction;
	private Function<Node, Void> permissionToShow;

	public SettingsPane(EventHandler<ActionEvent> backButtonAction) {
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SettingsForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

	}
	
	@FXML
	void showPeriodData(ActionEvent event) {
		if (permissionToShow != null)
			permissionToShow.apply(new PeriodDataPane(backButtonAction));
	}
	
	@FXML
	void showAboutPane(ActionEvent event) {
		if (permissionToShow != null)
			permissionToShow.apply(new AboutPane(backButtonAction));
	}

	void givePermission(Function<Node, Void> toShow) {
		permissionToShow = toShow;
	}

}
