package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.BiPredicate;
import java.util.function.Function;

import org.pstgu.WarehouseOrganizer.InfoModel.DishRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.DishRepositoryBinder;

import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

public class CookbookPane extends BorderPane implements Initializable {
	@FXML
	private Button backButton;
	@FXML
	private ComboBox<String> ingredientComboBox;
	@FXML
	private TableColumn<Dish, String> ingredientsCol;
	@FXML
	private TableColumn<Dish, String> nameCol;
	@FXML
	private TableView<Dish> tableView;
	@FXML
	private TableColumn<Dish, String> typeCol;
	@FXML
	private ComboBox<String> typeComboBox;

	private EventHandler<ActionEvent> backButtonAction;
	private DishRepositoryBinder model;
	private Function<Node, Void> permissionToShow;

	public CookbookPane(DishRepository cookbookRepository, EventHandler<ActionEvent> backButtonAction) {
		model = new DishRepositoryBinder(cookbookRepository);
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CookbookForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		prefTable();

		ObservableList<String> types = model.getDishTypesList();
		types.add(0, null);
		typeComboBox.setItems(types);
		ObservableList<String> ingredients = model.getAllDishIngredientsList();
		ingredients.add(0, null);
		ingredientComboBox.setItems(ingredients);

	}

	private void prefTable() {

		TableColumnFactory.prefStringColumn(typeCol, (Dish d) -> model.dishTypeProperty(d));

		Callback<TableColumn<Dish, String>, TableCell<Dish, String>> cellFactory = new Callback<TableColumn<Dish, String>, TableCell<Dish, String>>() {
			@Override
			public TableCell<Dish, String> call(final TableColumn<Dish, String> param) {
				final TableCell<Dish, String> cell = new TableCell<Dish, String>() {
					private final Button btn = new Button("Посмотреть");

					{ // constructor for NoNameClass
						btn.setOnAction((ActionEvent event) -> {
							Dish dish = getTableView().getItems().get(getIndex());
							DishViewPane dishViewPane = new DishViewPane(dish, model, backButtonAction);
							if (permissionToShow != null)
								permissionToShow.apply(dishViewPane);
							// System.out.println("selectedDish: " + dish);
						});
					}

					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							BorderPane graphic = new BorderPane(null, null, btn, null,
									new Label(getTableView().getItems().get(getIndex()).getName()));
							setGraphic(graphic);
						}
					}

				};
				return cell;
			}
		};

		nameCol.setCellFactory(cellFactory);

		TableColumnFactory.prefStringColumn(ingredientsCol, (Dish d) -> model.getIngredientsOfDishProperty(d));

		// Map<ObjectProperty<String>, List<BiPredicate<Dish, String>>> filters = new
		// HashMap<>();
		// filters.put(typeComboBox.valueProperty(),
		// Arrays.asList((dish, filterForDishType) ->
		// dish.getType().toLowerCase().indexOf(filterForDishType) != -1));
		// filters.put(ingredientComboBox.valueProperty(),
		// Arrays.asList((dish, filterForIngredient) ->
		// model.getDishIngredientsProperty(dish).getValue().toLowerCase().indexOf(filterForIngredient)
		// != -1));
		//
		// tableView.setItems(
		// FilteringFactory.wrapInFilteredList(filters, model.getDishList(),
		// tableView.comparatorProperty()));

		Map<ObservableValue<String>, List<BiPredicate<Dish, String>>> filters = new HashMap<>();

		filters.put(typeComboBox.valueProperty(),
				Arrays.asList((dish, requestString) -> dish.getType().toLowerCase().indexOf(requestString) != -1));
		filters.put(ingredientComboBox.valueProperty(), Arrays.asList((dish, requestString) -> model
				.getIngredientsOfDishProperty(dish).getValue().toLowerCase().indexOf(requestString) != -1));

		tableView.setItems(
				FilteringFactory.wrapInFilteredList(filters, model.getDishList(), tableView.comparatorProperty()));

	}

	void givePermission(Function<Node, Void> toShow) {
		permissionToShow = toShow;
	}

}