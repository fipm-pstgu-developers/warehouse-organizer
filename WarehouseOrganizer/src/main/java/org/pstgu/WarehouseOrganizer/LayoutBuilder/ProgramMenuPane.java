package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public class ProgramMenuPane extends BorderPane implements Initializable {

	@FXML
	private Button SettingsBtn;
	@FXML
	private Button ordersBtn;
	@FXML
	private Button productsBtn;
	@FXML
	private Button cookbookBtn;
	@FXML
	private Button menuBtn;
	@FXML
	private Button tasksTodayBtn;
	@FXML
	private Button deliverySheduleBtn;

	private EventHandler<ActionEvent> showTasksToday;
	private EventHandler<ActionEvent> showFoodInStock;
	private EventHandler<ActionEvent> showOrders;
	private EventHandler<ActionEvent> showCookbook;
	private EventHandler<ActionEvent> showMenu;
	private EventHandler<ActionEvent> showDeliveryShedule;
	private EventHandler<ActionEvent> showSettings;

	public ProgramMenuPane() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ProgramMenuForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) { // call after initialize all class @FXML fields
	}

	@FXML
	void showTasksToday(ActionEvent event) {
		if (showTasksToday != null)
			showTasksToday.handle(event);
	}

	@FXML
	void showFoodInStock(ActionEvent event) {
		if (showFoodInStock != null)
			showFoodInStock.handle(event);
	}

	@FXML
	void showOrders(ActionEvent event) {
		if (showOrders != null)
			showOrders.handle(event);
	}

	@FXML
	void showCookbook(ActionEvent event) {
		if (showCookbook != null)
			showCookbook.handle(event);
	}

	@FXML
	void showSettings(ActionEvent event) {
		if (showSettings != null)
			showSettings.handle(event);
	}

	@FXML
	void showMenu(ActionEvent event) {
		if (showMenu != null)
			showMenu.handle(event);
	}

	@FXML
	void showDeliveryShedule(ActionEvent event) {
		if (showDeliveryShedule != null)
			showDeliveryShedule.handle(event);
	}

	public void setToShowTasksToday(EventHandler<ActionEvent> handler) {
		showTasksToday = handler;
	}

	public void setToShowFoodInStock(EventHandler<ActionEvent> handler) {
		showFoodInStock = handler;
	}

	public void setToShowOrders(EventHandler<ActionEvent> handler) {
		showOrders = handler;
	}

	public void setToShowCookbook(EventHandler<ActionEvent> handler) {
		showCookbook = handler;
	}

	public void setToShowSettings(EventHandler<ActionEvent> handler) {
		showSettings = handler;
	}

	public void setToShowMenu(EventHandler<ActionEvent> handler) {
		showMenu = handler;
	}

	public void setToShowDeliverySchedule(EventHandler<ActionEvent> handler) {
		showDeliveryShedule = handler;
	}

}
