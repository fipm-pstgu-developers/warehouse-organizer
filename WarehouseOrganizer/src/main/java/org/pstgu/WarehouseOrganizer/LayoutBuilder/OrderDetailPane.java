package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.pstgu.WarehouseOrganizer.Application.Controller;
import org.pstgu.WarehouseOrganizer.InfoModel.NotFoundException;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.OrderRepositoryBinder;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

public class OrderDetailPane extends BorderPane implements Initializable {

	@FXML
	private Button backButton;
	@FXML
	private Button saveOrdProductsBtn;

	@FXML
	private BorderPane providerPane;
	@FXML
	private Label providerLbl;
	@FXML
	private ComboBox<String> providerComboBox;

	@FXML
	private Label titleLbl;

	@FXML
	private TableColumn<OrdProduct, String> actualNumberOfCol;
	@FXML
	private TableColumn<OrdProduct, String> commentCol;
	@FXML
	private TableColumn<OrdProduct, String> expectedNumberOfCol;
	@FXML
	private TableColumn<OrdProduct, Void> operationsCol;
	@FXML
	private TableColumn<OrdProduct, String> productNameCol;

	@FXML
	private TableView<OrdProduct> tableView;
	@FXML
	private GridPane gridPane;

	@FXML
	private StackPane stackPane;

	public static final String BE_DELIVERED = "доставлен";
	public static final String BE_NOT_DELIVERED = "не доставлен";

	private Order order;
	private OrderRepositoryBinder modelBinder;
	private Controller controller;
	private Logger log = Logger.getLogger(getClass().getName());

	private EventHandler<ActionEvent> backButtonAction;

	public OrderDetailPane(Order order, OrderRepositoryBinder modelBinder, Controller controller,
			EventHandler<ActionEvent> backButtonAction) {
		this.order = order;
		this.modelBinder = modelBinder;
		this.controller = controller;
		this.backButtonAction = backButtonAction;
		// this.productRepository = productRepository;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("OrderDetailForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		titleLbl.setText("ЗАКАЗ " + order.getCreationDate().format(DateTimeFormatter.ofPattern("dd.MM")));

		if (!order.getProvider().isEmpty()) {
			providerLbl.setText(order.getProvider().getDisplayName());
		} else {
			providerComboBox.setVisible(true);
			providerComboBox.setItems(modelBinder.getProviders().stream().map(provider -> provider.getDisplayName())
					.sorted().collect(Collectors.toCollection(FXCollections::observableArrayList)));
			providerLbl.textProperty().bind(providerComboBox.valueProperty());
			providerComboBox.valueProperty().addListener((o, oldVal, newVal) -> {
				if (!newVal.isEmpty()) {
					try {
						Provider provider = modelBinder.getProvider(newVal.split("\t")[0]);
						order.setProvider(provider);
						modelBinder.updateOrder(order);
					} catch (NotFoundException e) {
						e.printStackTrace();
						log.log(Level.WARNING, e.getMessage());
					}
				}
			});
		}

		if (!order.getComment().contains(BE_DELIVERED) && order.getSendingDate() != null) {
			saveOrdProductsBtn.setOnAction((e) -> {
				controller.accept(order);
				order.setComment("Заказ " + BE_DELIVERED);
				modelBinder.updateOrder(order);
				backButtonAction.handle(e);
			});
		} else {
			saveOrdProductsBtn.setVisible(false);
		}

		gridPane.maxWidthProperty().bind(tableView.widthProperty().subtract(operationsCol.widthProperty()));

		ColumnConstraints c0 = new ColumnConstraints();
		c0.prefWidthProperty().bind(productNameCol.widthProperty());
		gridPane.getColumnConstraints().add(c0);

		ColumnConstraints c1 = new ColumnConstraints();
		c1.prefWidthProperty().bind(expectedNumberOfCol.widthProperty());
		gridPane.getColumnConstraints().add(c1);

		ColumnConstraints c2 = new ColumnConstraints();
		c2.prefWidthProperty().bind(actualNumberOfCol.widthProperty());
		gridPane.getColumnConstraints().add(c2);

		ColumnConstraints c3 = new ColumnConstraints();
		c3.prefWidthProperty().bind(commentCol.widthProperty());
		gridPane.getColumnConstraints().add(c3);

		boolean emptyOrder = order.getProducts().isEmpty() || order.getProducts().get(0).isEmpty() ? true : false;
		prefTable(emptyOrder);
	}

	private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
		for (Node node : gridPane.getChildren()) {
			Integer ci = GridPane.getColumnIndex(node);
			Integer ri = GridPane.getRowIndex(node);
			if (ci != null && ci == col && ri != null && ri == row) {
				return node;
			}
		}
		return null;
	}

	static void deleteRow(GridPane grid, final int row) {
		Set<Node> deleteNodes = new HashSet<>();
		for (Node child : grid.getChildren()) {
			// get index from child
			Integer rowIndex = GridPane.getRowIndex(child);

			// handle null values for index=0
			int r = rowIndex == null ? 0 : rowIndex;

			if (r > row) {
				// decrement rows for rows after the deleted row
				GridPane.setRowIndex(child, r - 1);
			} else if (r == row) {
				// collect matching rows for deletion
				deleteNodes.add(child);
			}
		}

		// remove nodes from row
		grid.getChildren().removeAll(deleteNodes);
	}

	private void prefTable(boolean emptyOrder) {
		TableColumnFactory.prefStringColumn(productNameCol, (OrdProduct p) -> modelBinder.productNameProp(p));
		TableColumnFactory.prefStringColumn(expectedNumberOfCol, (OrdProduct p) -> modelBinder.expectedNumberOfProp(p));
		TableColumnFactory.prefStringColumn(actualNumberOfCol, (OrdProduct p) -> modelBinder.actualNumberOfProp(p));
		TableColumnFactory.prefStringColumn(commentCol, (OrdProduct p) -> modelBinder.ordProductCommentProp(p));

		Callback<TableColumn<OrdProduct, Void>, TableCell<OrdProduct, Void>> cellFactory = new Callback<TableColumn<OrdProduct, Void>, TableCell<OrdProduct, Void>>() {
			@Override
			public TableCell<OrdProduct, Void> call(final TableColumn<OrdProduct, Void> param) {
				final TableCell<OrdProduct, Void> cell = new TableCell<OrdProduct, Void>() {
					private final ToggleButton editBtn = new ToggleButton("[ред]");

					{ // constructor for NoNameClass
						editBtn.setOnAction((ActionEvent event) -> {
							OrdProduct product = getTableView().getItems().get(getIndex());
							if (editBtn.isSelected()) {

								Button plus = new Button("+");
								plus.getStylesheets().add("button1");

								plus.setOnAction((ActionEvent evnt) -> {
									Integer ri = GridPane.getRowIndex(((Button) evnt.getSource()).getParent());
									if (ri == null)
										return;
									ComboBox<String> selectProductCB = new ComboBox<>(
											FXCollections.observableArrayList(modelBinder.availableProducts().stream()
													.map(Product::getName).collect(Collectors.toList())));
									Button minus = new Button("-");
									minus.getStylesheets().add("button1");
									minus.setOnAction((ActionEvent e) -> {
										Integer ri2 = GridPane.getRowIndex(((Button) e.getSource()).getParent());
										if (ri2 == null)
											return;
										deleteRow(gridPane, ri2.intValue());
									});
									Label expNumOfLbl = new Label();
									TextField actNumOfLbl = new TextField("0");
									expNumOfLbl.textProperty().bind(actNumOfLbl.textProperty());

									int rowCount = getRowCount(gridPane);
									gridPane.add(new HBox(selectProductCB, minus), 0, rowCount);
									gridPane.add(expNumOfLbl, 1, rowCount);
									gridPane.add(actNumOfLbl, 2, rowCount);
									gridPane.add(new TextField(), 3, rowCount);
								});

								Button minus = new Button("-");
								minus.getStylesheets().add("button1");
								minus.setOnAction((ActionEvent evnt) -> {
									Integer ri = GridPane.getRowIndex(((Button) evnt.getSource()).getParent());
									if (ri == null)
										return;
									deleteRow(gridPane, ri.intValue());
								});

								if (!emptyOrder) {
									gridPane.add(new Label(product.getExpectedNumberOf().toString()), 1, 0);
									gridPane.add(new TextField(product.getActualNumberOf().toString()), 2, 0);
									gridPane.add(new TextField(product.getComment()), 3, 0);

									Label productNameLbl = new Label(product.getProductName());
									gridPane.add(new HBox(productNameLbl, plus, minus), 0, 0);
								} else {
									gridPane.add(new HBox(new Label(), plus, minus), 0, 0);
								}

								Bounds editBtnBounds = tableView.localToParent(editBtn.getBoundsInParent());
								StackPane.setMargin(gridPane, new Insets(editBtnBounds.getMaxY(), 0, 0, 0));
								gridPane.toFront();

							} else {

								int i = 0;
								int rowCount = getRowCount(gridPane);
								if (rowCount > 0) {

									if (((HBox) getNodeFromGridPane(gridPane, 0, 0)).getChildren().get(0)
											.getClass() == Label.class) {
										if (!emptyOrder) {
											product.setActualNumberOf(Double.parseDouble(
													((TextField) getNodeFromGridPane(gridPane, 2, 0)).getText()));
											product.setComment(
													((TextField) getNodeFromGridPane(gridPane, 3, 0)).getText());
											modelBinder.updateOrdProduct(product);
										}
										i = 1;
									}

									for (; i < rowCount; i++) {
										try {
											String productNewName = (String) ((ComboBox) ((HBox) getNodeFromGridPane(
													gridPane, 0, i)).getChildren().get(0)).getValue();
											if (productNewName == null)
												continue;

											Product prod = modelBinder.getProduct(productNewName);
											double actualNumberOf = Double.parseDouble(
													((TextField) getNodeFromGridPane(gridPane, 2, i)).getText());
											String comment = ((TextField) getNodeFromGridPane(gridPane, 3, i))
													.getText();

											OrdProduct newOrdProd = new OrdProduct(-1, prod, actualNumberOf,
													actualNumberOf, comment);
											newOrdProd.setOrderId(order.getId());

											modelBinder.addOrdProduct(newOrdProd);
										} catch (NotFoundException e) {
											log.log(Level.INFO, e.getMessage());
										}
									}
								} else {
									try {
										modelBinder.deleteOrdProduct(product);
									} catch (NotFoundException e) {
										e.printStackTrace();
									}
								}

								gridPane.getChildren().clear();
								updateTableItems();

								tableView.toFront();
							}
							// editBtn.setOnAction();
						});

					}

					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(new BorderPane(editBtn));
						}
					}

				};
				return cell;
			}
		};
		operationsCol.setCellFactory(cellFactory);

		tableView.setItems(FXCollections.observableList(order.getProducts()));
	}

	private int getRowCount(GridPane pane) {
		int numRows = pane.getRowConstraints().size();
		for (Node child : pane.getChildren()) {
			if (child.isManaged()) {
				Integer rowIndex = GridPane.getRowIndex(child);
				if (rowIndex != null) {
					numRows = Math.max(numRows, rowIndex + 1);
				}
			}
		}
		return numRows;
	}

	public void updateTableItems() {
		tableView.setItems(modelBinder.getProducts(order));
		tableView.refresh();
	}

}
