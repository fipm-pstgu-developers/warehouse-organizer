package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

import org.pstgu.WarehouseOrganizer.InfoModel.RepositoryManager;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.GiveOutTask;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Task;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.OrderRepositoryBinder;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;

public class TasksTodayPane extends BorderPane implements Initializable {

	@FXML
	private Button backButton;
//	@FXML
//	private Button breadButton;
//	@FXML
//	private Button breakfestButton;
//	@FXML
//	private Button dinnerButton;
//	@FXML
//	private Button outDinnerButton;
//	@FXML
//	private Button outPreDinnerButton;
//	@FXML
//	private Button ordersForTodayButton;
	@FXML
	private TilePane tasksPane;

	private Preferences userPrefs;
	private ObservableList<Task> tasks;
	private ObservableList<Task> completedTasks;
	private RepositoryManager model;
	private EventHandler<ActionEvent> backButtonAction;

	private Function<Node, Void> permissionToShow;

	public TasksTodayPane(ObservableList<Task> tasks, ObservableList<Task> completedTasks, RepositoryManager manager,
			EventHandler<ActionEvent> backButtonAction) {
		this.tasks = tasks;
		this.completedTasks = completedTasks;
		this.model = manager;
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TasksTodayForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		for (Task task : tasks) {
			CheckBox complete = new CheckBox();
			complete.setSelected(task.isCompleted());
			complete.addEventFilter(Event.ANY, event -> event.consume());

			Button taskBtn = new Button(task.getName(), complete);
			taskBtn.getStyleClass().add("task-button");
			tasksPane.getChildren().add(taskBtn);

			EventHandler<ActionEvent> executeTaskAction = e -> {
				task.run();
				complete.setSelected(true);
				backButtonAction.handle(e);
			};
			taskBtn.setOnAction(e -> {
				Pane pane = null;
				switch (task.getType()) {
				case GIVE_OUT_FOOD:
					pane = new FoodGiveOutPane(model.getMenuRepository(), model.getProductRepository(),
							(GiveOutTask) task, executeTaskAction, backButtonAction);
					break;
				case BREAD_ORDERS:
					pane = new OrdersDetailPane(model.getOrderRepository().getUnsentBreadOrders(),
							new OrderRepositoryBinder(model.getOrderRepository(), model.getProductRepository()),
							executeTaskAction, backButtonAction);
					break;
				case DAILY_ORDERS:
					pane = new OrdersDetailPane(model.getOrderRepository().getUnsentNotBreadOrders(),
							new OrderRepositoryBinder(model.getOrderRepository(), model.getProductRepository()),
							executeTaskAction, backButtonAction);
					break;
				default:
					break;
				}

//				DishViewPane dishViewPane = new DishViewPane(dish, model, backButtonAction);
				if (permissionToShow != null)
					permissionToShow.apply(pane);
			});
		}

	}

	void givePermission(Function<Node, Void> toShow) {
		permissionToShow = toShow;
	}
}
