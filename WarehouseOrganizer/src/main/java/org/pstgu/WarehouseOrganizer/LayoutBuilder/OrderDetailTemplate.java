package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.OrderRepositoryBinder;

import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

public class OrderDetailTemplate extends BorderPane implements Initializable {

	@FXML
	private Label providerLbl;
	@FXML
	private Button makeOrderBtn;
	@FXML
	private TableColumn<OrdProduct, String> actualNumberOfCol;
	@FXML
	private TableColumn<OrdProduct, String> commentCol;
	@FXML
	private TableColumn<OrdProduct, String> expectedNumberOfCol;
	@FXML
	private TableColumn<OrdProduct, Void> operationsCol;
	@FXML
	private TableColumn<OrdProduct, String> productNameCol;

	@FXML
	private TableView<OrdProduct> tableView;

	private Order order;

	private Function<Order, Void> sendOrder;
	OrderRepositoryBinder modelBinder;
	EventHandler<ActionEvent> backButtonAction;
	private Function<Node, Void> permissionToShow;


	public OrderDetailTemplate(Order order, OrderRepositoryBinder modelBinder) {
		this.order = order;
		this.modelBinder = modelBinder;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("OrderDetailTemplate.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		providerLbl.setText(order.getProvider().getName() + "\t [" + order.getProvider().getPhone() + "]");

		prefTable();

		if(permissionToShow != null) {
			permissionToShow.apply(this);
		}
	}

	private void prefTable() {
		TableColumnFactory.prefStringColumn(productNameCol, (OrdProduct p) -> modelBinder.productNameProp(p));
		TableColumnFactory.prefStringColumn(expectedNumberOfCol, (OrdProduct p) -> modelBinder.expectedNumberOfProp(p));
		TableColumnFactory.prefStringColumn(actualNumberOfCol, (OrdProduct p) -> modelBinder.actualNumberOfProp(p));
		TableColumnFactory.prefStringColumn(commentCol, (OrdProduct p) -> modelBinder.ordProductCommentProp(p));

		PseudoClass editableCssClass = PseudoClass.getPseudoClass("editable");

		Callback<TableColumn<OrdProduct, Void>, TableCell<OrdProduct, Void>> cellFactory = new Callback<TableColumn<OrdProduct, Void>, TableCell<OrdProduct, Void>>() {
			@Override
			public TableCell<OrdProduct, Void> call(final TableColumn<OrdProduct, Void> param) {
				final TableCell<OrdProduct, Void> cell = new TableCell<OrdProduct, Void>() {
					private final Button editBtn = new Button("[ред]");

					{ // constructor for NoNameClass
						editBtn.setOnAction((ActionEvent event) -> {
							OrdProduct p = getTableView().getItems().get(getIndex());
							// editBtn.setOnAction();
						});
					}

					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(new BorderPane(editBtn));
						}
					}

				};
//				cell.itemProperty().addListener((obs, oldValue, newValue) -> {
//	                TableRow row = cell.getTableRow();
//	                if (row == null) {
//	                    cell.setEditable(false);
//	                } else {
//	                    Void item = (Void) cell.getTableRow().getItem();
//	                    if (item == null) {
//	                        cell.setEditable(false);
//	                    } else {
//	                        cell.setEditable(true);
//	                    }
//	                }
//	                cell.pseudoClassStateChanged(editableCssClass, cell.isEditable());
//	            });
				return cell;
			}
		};
		operationsCol.setCellFactory(cellFactory);

		tableView.setItems(modelBinder.getProducts(order));
	}

	@FXML
	void makeOrder(ActionEvent event) {
		if (sendOrder != null) {
			sendOrder.apply(order);
		}
		tableView.setItems(modelBinder.getProducts(order));
	}

	void setToSendOrder(Function<Order, Void> action) {
		this.sendOrder = action;
	}

	void givePermission(Function<Node, Void> toShow) {
		permissionToShow = toShow;
	}

}
