package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.pstgu.WarehouseOrganizer.InfoModel.MenuRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.NotFoundException;
import org.pstgu.WarehouseOrganizer.InfoModel.ProductRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.GiveOutTask;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;

public class FoodGiveOutPane extends BorderPane implements Initializable {

	@FXML
	private Button backButton;

	@FXML
	private Button giveOutBtn;

	@FXML
	private TableView<Dish> giveOutTab;

	@FXML
	private TableColumn<Dish, String> giveOutTabColComment;

	@FXML
	private TableColumn<Dish, String> giveOutTabColMeasure;

	@FXML
	private TableColumn<Dish, String> giveOutTabColMeasureFact;

	@FXML
	private TableColumn<Dish, String> giveOutTabColName;

	@FXML
	private TableColumn<Dish, String> giveOutTabColOperations;

	@FXML
	private TableColumn<Dish, String> giveOutTabColProduct;

	@FXML
	private Label nameLabel;

	@FXML
	private GridPane gridPane;

	@FXML
	private StackPane stackPane;
	@FXML
	private BorderPane headerPane;

	private EventHandler<ActionEvent> backButtonAction;
	private MenuRepository model;
	private ProductRepository productRepository;
	private GiveOutTask task;
	private EventHandler<ActionEvent> executeTaskAction;
	private Logger log = Logger.getLogger(getClass().getName());

	public FoodGiveOutPane(MenuRepository menuRepository, ProductRepository productRepository, GiveOutTask task,
			EventHandler<ActionEvent> executeTaskAction, EventHandler<ActionEvent> backButtonAction) {
		model = menuRepository;
		this.productRepository = productRepository;
		this.task = task;
		this.executeTaskAction = executeTaskAction;
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FoodGiveOutForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		backButton.setOnAction(backButtonAction);

		prefTable();

		try {
			if (!model.get(LocalDate.now()).getDishes().get(task.getFoodReception()).stream()
					.map(dish -> dish.isGaveOut()).reduce((e1, e2) -> e1 && e2).orElse(true)) {
				giveOutBtn.setOnAction(executeTaskAction);
			} else {
				giveOutBtn.setVisible(false);
			}
		} catch (NotFoundException e) {
			e.printStackTrace();
		}

		nameLabel.setText(task.getName().toUpperCase());

	}

	private void prefCol(TableColumn<Dish, String> col, Function<Dish, String> factory) {
		col.setCellValueFactory(new Callback<CellDataFeatures<Dish, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Dish, String> param) {
				Dish myDish = param.getValue();
				return new SimpleStringProperty(factory.apply(myDish));

			}
		});

	}

	private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
		for (Node node : gridPane.getChildren()) {
			Integer ci = GridPane.getColumnIndex(node);
			Integer ri = GridPane.getRowIndex(node);
			if (ci != null && ci == col && ri != null && ri == row) {
				return node;
			}
		}
		return null;
	}

	static void deleteRow(GridPane grid, final int row) {
		Set<Node> deleteNodes = new HashSet<>();
		for (Node child : grid.getChildren()) {
			// get index from child
			Integer rowIndex = GridPane.getRowIndex(child);

			// handle null values for index=0
			int r = rowIndex == null ? 0 : rowIndex;

			if (r > row) {
				// decrement rows for rows after the deleted row
				GridPane.setRowIndex(child, r - 1);
			} else if (r == row) {
				// collect matching rows for deletion
				deleteNodes.add(child);
			}
		}

		// remove nodes from row
		grid.getChildren().removeAll(deleteNodes);
	}

	private void prefTable() {

		try {
			List<Dish> list = model.get(LocalDate.now()).getDishes().get(task.getFoodReception());
			giveOutTab.setItems(FXCollections.observableArrayList(list));
		} catch (NotFoundException e) {
			e.printStackTrace();
		}

		giveOutTabColName.setCellValueFactory(new PropertyValueFactory<>("name"));

		prefCol(giveOutTabColProduct, new Function<Dish, String>() {
			@Override
			public String apply(Dish myDish) {
				return myDish.getIngredients().stream().map(ingredient -> ingredient.getProduct().getName())
						.collect(Collectors.joining("\n")).toString();
			}
		});

		prefCol(giveOutTabColMeasure, new Function<Dish, String>() {
			@Override
			public String apply(Dish myDish) {
				return myDish.getIngredients().stream()
						.map(ingredient -> Double.toString(ingredient.getExpectedNumberOf()) + " "
								+ ingredient.getProduct().getMeasureUnit())
						.collect(Collectors.joining("\n")).toString();
			}
		});

		prefCol(giveOutTabColMeasureFact, new Function<Dish, String>() {
			@Override
			public String apply(Dish myDish) {
				return myDish.getIngredients().stream()
						.map(ingredient -> Double.toString(ingredient.getActualNumberOf()) + " "
								+ ingredient.getProduct().getMeasureUnit())
						.collect(Collectors.joining("\n")).toString();
			}
		});

		prefCol(giveOutTabColComment, new Function<Dish, String>() {
			@Override
			public String apply(Dish myDish) {
				return myDish.getIngredients().stream().map(ingredient -> ingredient.getComment())
						.collect(Collectors.joining("\n")).toString();
			}
		});

		gridPane.maxWidthProperty().bind(giveOutTab.widthProperty().subtract(giveOutTabColOperations.widthProperty()));

		ColumnConstraints c0 = new ColumnConstraints();
		c0.prefWidthProperty().bind(giveOutTabColName.widthProperty());
		gridPane.getColumnConstraints().add(c0);

		ColumnConstraints c1 = new ColumnConstraints();
		c1.prefWidthProperty().bind(giveOutTabColProduct.widthProperty());
		gridPane.getColumnConstraints().add(c1);

		ColumnConstraints c2 = new ColumnConstraints();
		c2.prefWidthProperty().bind(giveOutTabColMeasure.widthProperty());
		gridPane.getColumnConstraints().add(c2);

		ColumnConstraints c3 = new ColumnConstraints();
		c3.prefWidthProperty().bind(giveOutTabColMeasureFact.widthProperty());
		gridPane.getColumnConstraints().add(c3);

		ColumnConstraints c4 = new ColumnConstraints();
		c4.prefWidthProperty().bind(giveOutTabColComment.widthProperty());
		gridPane.getColumnConstraints().add(c4);

		Callback<TableColumn<Dish, String>, TableCell<Dish, String>> cellFactory = new Callback<TableColumn<Dish, String>, TableCell<Dish, String>>() {
			@Override
			public TableCell<Dish, String> call(final TableColumn<Dish, String> param) {
				final TableCell<Dish, String> cell = new TableCell<Dish, String>() {

					private final ToggleButton editRow = new ToggleButton("Редактировать");

					{
						editRow.setOnAction(evt -> {

							Dish dish = getTableView().getItems().get(getIndex());
							if (editRow.isSelected()) {

								int i = 0;
								for (DishIngredient ingr : dish.getIngredients()) {

									Button plus = new Button("+");
									plus.setOnAction((ActionEvent event) -> {
										Integer ri = GridPane.getRowIndex(((Button) event.getSource()).getParent());
										if (ri == null)
											return;
										// addRow(gridPane, ri.intValue());
										ComboBox<String> selectProductCB = new ComboBox<>(
												FXCollections.observableArrayList(productRepository.getAll().stream()
														.map(Product::getName).collect(Collectors.toList())));
										// selectProductCB.setValue(ingr.getProduct().getName());
										int j = getRowCount(gridPane);
										gridPane.add(selectProductCB, 1, j);
										TextField expectedNumberOf = new TextField();
										// ingr.getExpectedNumberOf().toString());
										gridPane.add(expectedNumberOf, 3, j);
										gridPane.add(new TextField(), 4, j);
										System.out.println(getRowCount(gridPane));
										// dish.getIngredients().add(ingr);

									});

									plus.getStylesheets().add("button1");
									Button minus = new Button("-");
									minus.setOnAction((ActionEvent event) -> {
										Integer ri = GridPane.getRowIndex(((Button) event.getSource()).getParent());
										if (ri == null)
											return;
										String productName = (String) ((ComboBox) getNodeFromGridPane(gridPane, 1,
												ri.intValue())).getValue();
										deleteIngredient(dish, productName);

										deleteRow(gridPane, ri.intValue());
										updateTableItems();
									});

									minus.getStylesheets().add("button1");
									TextField actualNumberOf = new TextField(ingr.getActualNumberOf().toString());
									gridPane.add(new HBox(plus, minus), 0, i);

									ComboBox<String> selectProductCB = new ComboBox<>(
											FXCollections.observableArrayList(productRepository.getAll().stream()
													.map(Product::getName).collect(Collectors.toList())));
									selectProductCB.setValue(ingr.getProduct().getName());
									gridPane.add(selectProductCB, 1, i);
									gridPane.add(actualNumberOf, 3, i);
									gridPane.add(new TextField(ingr.getComment()), 4, i);
									i++;

								}
								Bounds editBtnBounds = giveOutTab.localToParent(editRow.getBoundsInParent());
								StackPane.setMargin(gridPane, new Insets(editBtnBounds.getMaxY(), 0, 0, 0));

								gridPane.toFront();

							} else {
								int rowCount = getRowCount(gridPane);
								Set<String> allProdNames = new HashSet();
								for (int j = 0; j < rowCount; j++) {
									String productName = (String) ((ComboBox) getNodeFromGridPane(gridPane, 1, j))
											.getValue();
									allProdNames.add(productName);
								}
								Set<String> prodNames = dish.getIngredients().stream()
										.map(ingr -> ingr.getProduct().getName()).collect(Collectors.toSet());

								for (int i = 0; i < rowCount; i++) {
									String productName = (String) ((ComboBox) getNodeFromGridPane(gridPane, 1, i))
											.getValue();
									if (productName == null)
										continue;

									double actualNumberOf = Double
											.parseDouble(((TextField) getNodeFromGridPane(gridPane, 3, i)).getText());
									String comment = ((TextField) getNodeFromGridPane(gridPane, 4, i)).getText();

									boolean inProdNames = prodNames.contains(productName);
									boolean inAllProdNames = allProdNames.contains(productName);
									boolean inNewProdNames = inAllProdNames && !inProdNames;
									if (inProdNames && inAllProdNames) {
										// TODO update ingredient

										// altirnative code
//										DishIngredient dishIngredient = dish.getIngredients().stream()
//												.filter(dIngr -> dIngr.getProduct().getName().equals(productName))
//												.findFirst().get();
										DishIngredient dishIngredient = null;
										for (DishIngredient dingr : dish.getIngredients()) {
											if (dingr.getProduct().getName().equals(productName)) {
												dishIngredient = dingr;
											}
										}
										if (dishIngredient != null) {
											dishIngredient.setActualNumberOf(actualNumberOf);
											dishIngredient.setComment(comment);

											model.updateDishIngredient(dishIngredient);
										}

									} else if (inNewProdNames) {
										// add ingredient
										addIngredient(dish, productName, actualNumberOf, comment);
									}
									// delete action is binding on minus button action handler
								}

								gridPane.getChildren().clear();
								updateTableItems();
//								giveOutTab.refresh();

								giveOutTab.toFront();
							}

						});

					}

					private void deleteIngredient(Dish dish, String productName) {
						try {
							IntegerProperty dishIngrId = new SimpleIntegerProperty(-1);
							dish.getIngredients().stream().forEach(e -> {
								if (e.getProduct().getName().equals(productName)) {
									dishIngrId.set(e.getId());
								}
							});

							model.deleteDishIngredient(dishIngrId.get());
						} catch (NotFoundException ex) {
							log.log(Level.WARNING, ex.getMessage());
						}
					}

					private void addIngredient(Dish dish, String productName, double actualNumberOf, String comment) {
						try {
							Product newProduct = productRepository.get(productName);
							DishIngredient newDIngr = new DishIngredient(-1, newProduct, actualNumberOf, actualNumberOf,
									comment);
							newDIngr.setDishId(dish.getId());

							model.addDishIngredient(newDIngr);
						} catch (NotFoundException ex) {
							log.log(Level.WARNING, String.format("Product with name not found"));
						}
					}

					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {

							setGraphic(editRow);
						}
					}

				};
				return cell;

			}
		};

		giveOutTabColOperations.setCellFactory(cellFactory);
		updateTableItems();

	}

	private int getRowCount(GridPane pane) {
		int numRows = pane.getRowConstraints().size();
		for (Node child : pane.getChildren()) {
			if (child.isManaged()) {
				Integer rowIndex = GridPane.getRowIndex(child);
				if (rowIndex != null) {
					numRows = Math.max(numRows, rowIndex + 1);
				}
			}
		}
		return numRows;
	}

	private void updateTableItems() {
		giveOutTab.getItems().clear();
		// giveOutTab.setItems(model.);
		List<Dish> list;
		try {
			list = model.get(LocalDate.now()).getDishes().get(task.getFoodReception());
			// .get(task.getFoodReception());
			giveOutTab.setItems(FXCollections.observableArrayList(list));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
