package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.util.function.Function;

import org.pstgu.WarehouseOrganizer.Application.Controller;
import org.pstgu.WarehouseOrganizer.InfoModel.RepositoryManager;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;

public class ContentPane extends StackPane {

	private Function<Node, Void> permissionToShow;
	private EventHandler<ActionEvent> backButtonAction;
	private Controller controller;
	private RepositoryManager repositoryManager;

	private ProgramMenuPane programMenuPane;
	private SettingsPane settingsPane;
	private TasksTodayPane tasksTodayPane;
	private MenuPane menuPane;
	private OrderPane orderPane;
	private CookbookPane cookbookPane;
	private FoodInStockPane foodInStockPane;
	private DeliverySchedulePane deliverySchedulePane;
	private AuthorizationPane authorizationPane;

	public ContentPane(Controller controller, RepositoryManager repositoryManager) {
		this.controller = controller;
		this.repositoryManager = repositoryManager;
		permissionToShow = pane -> {
			getChildren().add(pane);
			return null;
		};
		backButtonAction = e -> getChildren().remove(getChildren().size() - 1);

		programMenuPane = new ProgramMenuPane();
		getChildren().add(programMenuPane);

		authorizationPane = new AuthorizationPane(backButtonAction);
		getChildren().add(authorizationPane);

//		programMenuPane = new ProgramMenuPane();
//		getChildren().add(programMenuPane);

		tasksTodayPane = new TasksTodayPane(controller.getTasks(), controller.getCompletedTasks(), repositoryManager,
				backButtonAction);
		tasksTodayPane.givePermission(permissionToShow);
		programMenuPane.setToShowTasksToday(event -> getChildren().add(tasksTodayPane));

		foodInStockPane = new FoodInStockPane(repositoryManager.getFoodRepository(), backButtonAction);
		programMenuPane.setToShowFoodInStock(event -> {
			foodInStockPane.update();
			getChildren().add(foodInStockPane);
		});

		orderPane = new OrderPane(repositoryManager.getOrderRepository(), repositoryManager.getProductRepository(),
				controller, backButtonAction);
		orderPane.givePermission(permissionToShow);
		programMenuPane.setToShowOrders(event -> {
			orderPane.update();
			getChildren().add(orderPane);
		});

		cookbookPane = new CookbookPane(repositoryManager.getDishRepository(), backButtonAction);
		cookbookPane.givePermission(permissionToShow);
		programMenuPane.setToShowCookbook(event -> getChildren().add(cookbookPane));

		menuPane = new MenuPane(repositoryManager.getMenuRepository(), backButtonAction);
		menuPane.givePermission(permissionToShow);
		programMenuPane.setToShowMenu(event -> getChildren().add(menuPane));

		settingsPane = new SettingsPane(backButtonAction);
		settingsPane.givePermission(permissionToShow);
		programMenuPane.setToShowSettings(event -> getChildren().add(settingsPane));

		deliverySchedulePane = new DeliverySchedulePane(repositoryManager.getOrderRepository(), backButtonAction);
		programMenuPane.setToShowDeliverySchedule(event -> getChildren().add(deliverySchedulePane));
	}

}