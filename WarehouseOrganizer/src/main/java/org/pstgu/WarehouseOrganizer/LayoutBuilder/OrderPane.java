package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

import org.pstgu.WarehouseOrganizer.Application.Controller;
import org.pstgu.WarehouseOrganizer.InfoModel.OrderRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.ProductRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;
import org.pstgu.WarehouseOrganizer.JavaFXBinder.OrderRepositoryBinder;

import javafx.beans.binding.ObjectBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

public class OrderPane extends BorderPane implements Initializable {

	@FXML
	private Button backButton;
	@FXML
	private Button createOrderBtn;
	@FXML
	private ComboBox<String> providerComboBox;
	@FXML
	private DatePicker datePicker;
	@FXML
	private ComboBox<String> statusComboBox;
	@FXML
	private TableView<Order> orderTable;
	@FXML
	private TextField filteredField;
	@FXML
	private TableColumn<Order, String> creationDateCol;
	@FXML
	private TableColumn<Order, String> providerNameCol;
	@FXML
	private TableColumn<Order, String> sendingDateCol;
	@FXML
	private TableColumn<Order, String> receiveDateCol;
	@FXML
	private TableColumn<Order, String> commentCol;

	private Preferences userPrefs;
	private EventHandler<ActionEvent> backButtonAction;
	private OrderRepositoryBinder model;
	private Controller controller;
	private Function<Node, Void> permissionToShow;

	public OrderPane(OrderRepository orderRepository, ProductRepository productRepository, Controller controller,
			EventHandler<ActionEvent> backButtonAction) {
//		table.editableProperty().bind(editableChB.selectedProperty());
//
//		content.getChildren().addAll(editableChB, table);

		model = new OrderRepositoryBinder(orderRepository, productRepository);
		this.controller = controller;
		this.backButtonAction = backButtonAction;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("OrderViewForm.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) { // call after initialize all class @FXML fields
		userPrefs = Preferences.userNodeForPackage(OrderPane.class);
		applyUserPrefs();

		prefTable();
		loadTableData();

		backButton.setOnAction(backButtonAction);
		createOrderBtn.setOnAction(event -> {
			Order empty = new Order(LocalDate.now(), null, "", Arrays.asList(new OrdProduct(-1, null, null, null, "")));
			model.addOrder(empty);
			OrderDetailPane pane = new OrderDetailPane(empty, model, controller, e -> {
				empty.setSendingDate(LocalDate.now());
				model.updateOrder(empty);
				backButtonAction.handle(e);
				update();
			});
			if (permissionToShow != null) {
				permissionToShow.apply(pane);
			}
		});

		providerComboBox.setItems(model.getProviders().stream().map(Provider::getName).sorted()
				.collect(Collectors.toCollection(FXCollections::observableArrayList)));
		providerComboBox.getItems().add(0, "");
	}

	private void applyUserPrefs() {
//		providerNameCol.setPrefWidth(Integer
//				.getInteger(userPrefs.get("providerNameColPrefWidth", "115"))); /* get stored width or default 115 */
//		providerNameCol.prefWidthProperty().addListener((l)->System.out.println("****"));

	}

	private void prefTable() {
		Callback<TableColumn<Order, String>, TableCell<Order, String>> cellFactory = new Callback<TableColumn<Order, String>, TableCell<Order, String>>() {
			@Override
			public TableCell<Order, String> call(final TableColumn<Order, String> param) {
				final TableCell<Order, String> cell = new TableCell<Order, String>() {
					private final Button openBtn = new Button();

					{ // constructor for NoNameClass
						openBtn.setOnAction((ActionEvent event) -> {
							TableView<Order> thisTableView = getTableView();
							Order order = thisTableView.getItems().get(getIndex());
							OrderDetailPane pane = new OrderDetailPane(order, model, controller, e -> {
								backButtonAction.handle(e);
								getTableRow().setItem(order);
								thisTableView.refresh();
							});
							if (permissionToShow != null) {
								pane.updateTableItems();
								permissionToShow.apply(pane);
							}
						});
						openBtn.getStyleClass().add("open-order-details-button");
					}

					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							openBtn.setText(getTableView().getItems().get(getIndex()).getCreationDate()
									.format(DateTimeFormatter.ofPattern("dd.MM")));
							setGraphic(openBtn);
						}
					}

				};
				return cell;
			}
		};

		creationDateCol.setCellFactory(cellFactory);

		TableColumnFactory.prefStringColumn(providerNameCol, (Order o) -> model.providerNameProperty(o));
		TableColumnFactory.prefStringColumn(receiveDateCol, (Order o) -> model.receiveDateProperty(o));
		TableColumnFactory.prefStringColumn(sendingDateCol, (Order o) -> model.sendingDateProperty(o));
		TableColumnFactory.prefStringColumn(commentCol, (Order o) -> model.orderCommentProperty(o));
	}

	private void loadTableData() {
		Map<ObservableValue<String>, List<BiPredicate<Order, String>>> filters = new HashMap<>();
		filters.put(providerComboBox.valueProperty(), Arrays.asList(
				(order, requestString) -> order.getProvider().getName().toLowerCase().indexOf(requestString) != -1));

		filters.put(statusComboBox.valueProperty(),
				Arrays.asList((order,
						requestString) -> order.getComment().contains(OrderDetailPane.BE_DELIVERED)
								&& requestString.equals(OrderDetailPane.BE_DELIVERED)
								|| !order.getComment().contains(OrderDetailPane.BE_DELIVERED)
										&& requestString.equals(OrderDetailPane.BE_NOT_DELIVERED)));

		ObjectBinding<String> sendingDateProperty = new ObjectBinding<String>() {
			{
				super.bind(datePicker.valueProperty());
			}

			@Override
			protected String computeValue() {
				if (datePicker.valueProperty().get() == null) {
					return "";
				}
				return datePicker.valueProperty().get().toString();
			}
		};
		filters.put(sendingDateProperty,
				Arrays.asList((order, requestString) -> order.getSendingDate() != null
						? order.getSendingDate().toString().equals(requestString)
						: false));

		orderTable.setItems(
				FilteringFactory.wrapInFilteredList(filters, model.getOrders(), orderTable.comparatorProperty()));

		// orderTable.setItems(model.getOrderList());
	}

	public void update() {
		loadTableData();
		orderTable.refresh();
	}

	void givePermission(Function<Node, Void> toShow) {
		permissionToShow = toShow;
	}

	@FXML
	void createOrder(ActionEvent event) {

	}
}
