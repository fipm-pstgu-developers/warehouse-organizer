package org.pstgu.WarehouseOrganizer.LayoutBuilder;

import java.util.function.Function;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class TableColumnFactory {
	private static StringConverter<String> sc = new StringConverter<String>() {
		@Override
		public String toString(String t) {
			return t == null ? null : t.toString();
		}

		@Override
		public String fromString(String string) {
			return string;
		}
	};

	private TableColumnFactory() {
	}

	public static <T> TableColumn<T, String> createStringColumn(String name,
			Function<T, ObservableValue<String>> stringPropertyGetter) {
		TableColumn<T, String> col = new TableColumn<T, String>(name);
		prefStringColumn(col, stringPropertyGetter);

		return col;
	}

	public static <T> void prefStringColumn(TableColumn<T, String> col,
			Function<T, ObservableValue<String>> stringPropertyGetter) {

		col.setCellValueFactory(new Callback<CellDataFeatures<T, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<T, String> f) {
				// f.getValue() returns the T instance for a particular TableView row
				return stringPropertyGetter.apply(f.getValue());
			}
		});
		col.setCellFactory(TextFieldTableCell.forTableColumn(sc));
	}

	public static <T> TableColumn<T, Boolean> createCheckBoxColumn(String name) {
		TableColumn<T, Boolean> col = new TableColumn<>(name);
		col.setCellValueFactory(new PropertyValueFactory<T, Boolean>("selected"));
		col.setCellFactory(CheckBoxTableCell.forTableColumn(col));

		return col;
	}
}
