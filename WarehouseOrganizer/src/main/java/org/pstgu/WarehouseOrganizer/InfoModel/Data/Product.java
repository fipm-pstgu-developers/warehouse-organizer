package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import java.util.Objects;

public class Product implements Comparable<Product> {

	private int id = -1;
	private String name;
	private String measureUnit;
	private double emergencyBalance = 1.0;

	public Product() {
	}

	public Product(int id, String name, String measureUnit) {
		this.id = id;
		this.name = name;
		this.measureUnit = measureUnit;
	}

	public Product(String name, String measureUnit) {
		this.name = name;
		this.measureUnit = measureUnit;
	}

	public Product(int id, String name, String measureUnit, double emergencyBalance) {
		super();
		this.id = id;
		this.name = name;
		this.measureUnit = measureUnit;
		this.emergencyBalance = emergencyBalance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMeasureUnit() {
		return measureUnit;
	}

	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}

	public double getEmergencyBalance() {
		return emergencyBalance;
	}

	public void setEmergencyBalance(double emergencyBalance) {
		this.emergencyBalance = emergencyBalance;
	}

	@Override
	public int hashCode() {
		return Objects.hash(measureUnit, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return measureUnit.equalsIgnoreCase(other.measureUnit) && name.equalsIgnoreCase(other.name);
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", measureUnit=" + measureUnit + ", emergencyBalance="
				+ emergencyBalance + "]";
	}

	@Override
	public int compareTo(Product o) {
		return getName().compareTo(o.getName());
	}

}
