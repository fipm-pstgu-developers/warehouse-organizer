package org.pstgu.WarehouseOrganizer.InfoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;

public class DishRepositoryImpl extends RepositoryImpl implements DishRepository {
	private Logger log = Logger.getLogger(getClass().getName());
	private Connection connection;
	ProductRepository productRepository;

	DishRepositoryImpl(Connection connection, ProductRepository productRepository) {
		this.connection = connection;
		this.productRepository = productRepository;
	}

	@Override
	public List<Dish> getAll() {
		// TODO bug fixing

		List<Dish> list = new ArrayList<>();
		try {
			String sql = "SELECT * FROM " + TBL_DISH + ";";
			Statement statement = connection.createStatement();
			ResultSet execResult = statement.executeQuery(sql);
			while (execResult.next()) {
				int dishId = execResult.getInt(F_ID);
				String dishName = execResult.getString("dishName");
				int dishTypeId = execResult.getInt("dishTypeId");
				String comment = execResult.getString(F_COMMENT);

				PreparedStatement preparedStatement = connection
						.prepareStatement("SELECT * FROM DishType WHERE id == ?");
				preparedStatement.setInt(1, dishTypeId);
				ResultSet typesSet = preparedStatement.executeQuery();
				if (typesSet.next() == false) {
					throw new NotFoundException("SELECT * FROM DishType WHERE id == ?");
				}

				Dish d = new Dish(dishId, dishName, typesSet.getString(F_TYPE), getDishIngredients(dishId), comment);
				list.add(d);
			}
			log.log(Level.INFO, String.format("had been GOT %d Dish(es)", list.size()));
		} catch (SQLException | NotFoundException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return list;
	}

	@Override
	public List<DishIngredient> getDishIngredients(int dishId) {
		// TODO bug fixing

		List<DishIngredient> list = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("SELECT * FROM DishIngredient WHERE dishId == ?");
			preparedStatement.setInt(1, dishId);
			ResultSet setOfDishIngredients = preparedStatement.executeQuery();
			while (setOfDishIngredients.next()) {
				int ingredientId = setOfDishIngredients.getInt(1);
				int productId = setOfDishIngredients.getInt(3);
				double expectedNumberOf = setOfDishIngredients.getDouble(4);
				double actualNumberOf = setOfDishIngredients.getDouble(5);
				String cookingType = setOfDishIngredients.getString(6);

				Product product = productRepository.get(productId);

				DishIngredient dishIngredient = new DishIngredient(ingredientId, product, expectedNumberOf,
						actualNumberOf, cookingType);
				list.add(dishIngredient);

			}
			log.log(Level.INFO, String.format("had been GOT %d dish ingredient(s)", list.size()));
		} catch (SQLException | NotFoundException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return list;
	}

	@Override
	public List<String> getAllTypesOfDishes() {
		// TODO bug fixing

		List<String> list = new ArrayList<>();
		try {
			String sql = "SELECT DISTINCT " + F_TYPE + " FROM " + TBL_DISH_TYPES + ";";
			Statement statement = connection.createStatement();
			ResultSet execResult = statement.executeQuery(sql);
			while (execResult.next()) {
				list.add(execResult.getString(1));
			}
			log.log(Level.INFO, String.format("had been GOT %d types of dishes(s)", list.size()));
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		Collections.sort(list);
		return list;
	}

	@Override
	public List<String> getAllIngredientsForDishes() {
		// TODO bug fixing

		List<String> list = new ArrayList<>();
		try {
			String sql = "SELECT DISTINCT " + F_PRODUCT_ID + " FROM " + TBL_DISH_INGREDIENTS + ";";
			Statement statement = connection.createStatement();
			ResultSet execResult = statement.executeQuery(sql);
			while (execResult.next()) {
				int productId = execResult.getInt(F_PRODUCT_ID);

				String productName = productRepository.get(productId).getName();

				list.add(productName);
			}
			log.log(Level.INFO, String.format("had been GOT %d ingredient(s)", list.size()));
		} catch (SQLException | NotFoundException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		Collections.sort(list);
		return list;
	}

	@Override
	public boolean add(Dish element) {
		// TODO bug fixing

		try {
			String sql = "INSERT INTO " + TBL_DISH + " (" + F_DISH_ID + ", " + F_DISH_NAME + ", " + F_DISH_TYPE_ID + " , " + F_COMMENT + " VALUES (?, ?, ?, ?);";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, element.getId());
			preparedStatement.setString(2, element.getName());
			preparedStatement.setString(3, element.getType());
			preparedStatement.setString(4, element.getComment());
			int rows = preparedStatement.executeUpdate();
			log.log(Level.INFO,
					String.format("had been ADDED element {%s, %s, %s, %s}", element.getId(), element.getName(), element.getType(), element.getComment()));
			return true;
		} catch (SQLException e) {
			log.log(Level.INFO, String.format("element {%s, %s, %s, %s} have been already exists", element.getId(), element.getName(), element.getType(), element.getComment()));


		}
		return false;
	}

	@Override
	public Dish get(Object key) throws NotFoundException {
		if (key.getClass() == String.class) {
			return get((String) key);
		}
		throw new NotFoundException("key type not found");
	}

	public Dish get(String name) throws NotFoundException {
		try {
			String sql = "SELECT * FROM " + TBL_DISH + " WHERE " + F_DISH_NAME + " = ?";
			PreparedStatement prepSt = connection.prepareStatement(sql);
			prepSt.setString(1, name);
			ResultSet resSet = prepSt.executeQuery();
			if (resSet.next() == false)
				throw new NotFoundException("Dish with name = " + name + "not found");

			int dishTypeId = resSet.getInt(F_DISH_TYPE_ID);
			String type = getDishType(dishTypeId);

			List<DishIngredient> ingredients = getDishIngredients(resSet.getInt(F_ID));

			Dish dish = new Dish(resSet.getInt(F_ID), resSet.getString(F_DISH_NAME), type, ingredients,
					resSet.getString(F_COMMENT));

			return dish;
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}

	}

	private String getDishType(int dishTypeId) throws NotFoundException {
		try {
			String sql = "SELECT " + F_TYPE + " FROM " + TBL_DISH_TYPES + " WHERE " + F_ID + " = ?";

			PreparedStatement prepSt = connection.prepareStatement(sql);
			prepSt.setInt(1, dishTypeId);
			ResultSet resSet = prepSt.executeQuery();
			if (resSet.next() == false)
				throw new NotFoundException("DishType with id = " + dishTypeId + "not found");

			return resSet.getString(F_TYPE);
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	@Override
	public Dish delete(Object key) throws NotFoundException {
		// TODO bug fixing

		if (key.getClass() != String.class) {
			throw new NotFoundException("key type not found");
		}

		String name = (String) key;
		try {
			Dish p = get(name);
			String sql = "DELETE FROM " + TBL_DISH + " WHERE " + F_DISH_NAME + " = ?;";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, name);
			int rows = preparedStatement.executeUpdate();
			log.log(Level.INFO, String.format("had been DELETED %d element %s", rows, p));
			return p;
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
		//return null;
	}

	@Override
	public boolean update(Dish element) {
		// TODO bug fixing

		try {
			String sql = "UPDATE " + TBL_DISH + " SET " + F_DISH_NAME + " = ?, " + F_DISH_TYPE_ID + "  = ?, " + F_COMMENT + " =? WHERE " + F_DISH_ID
					+ " = ?;";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, element.getName());
			preparedStatement.setString(2, element.getType());
			preparedStatement.setString(3, element.getComment());
			preparedStatement.setInt(4, element.getId());
			int rows = preparedStatement.executeUpdate();
			if (rows == 0)
				throw new NotFoundException();
			log.log(Level.INFO, String.format("had been UPDATED %d element %s", rows, element));
			return true;
		} catch (NotFoundException nfe) {
			log.log(Level.WARNING, "Dish with id = " + element.getId() + " not found.");
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return false;
	}

}
