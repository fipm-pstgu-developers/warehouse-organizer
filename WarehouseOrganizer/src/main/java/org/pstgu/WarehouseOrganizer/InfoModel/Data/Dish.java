package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import java.util.List;
import java.util.Objects;

/*
	-- Тип: супы, гарниры, напитки
	CREATE TABLE IF NOT EXISTS DishType (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		type TEXT NOT NULL UNIQUE
	);

	-- Ингредиент блюда
	CREATE TABLE IF NOT EXISTS DishIngredient (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		productId INTEGER,
		numberOf REAL,
		cookingType TEXT,
		FOREIGN KEY (productId) REFERENCES Product (id)
	);

	-- Блюдо из справочника блюд
	CREATE TABLE IF NOT EXISTS Dish (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		dishName TEXT NOT NULL UNIQUE,
		dishTypeId INTEGER,
		FOREIGN KEY (dishTypeId) REFERENCES DishType (id)
	);
 */
public class Dish {

	private int id = -1;
	private String name;
	private String type;
	private List<DishIngredient> ingredients;
	private String comment;
	private boolean givenOut = false;

	public Dish(int id, String name, String type, List<DishIngredient> ingredients, String comment) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.ingredients = ingredients;
		this.comment = comment;
	}

	public Dish(String name, String type, List<DishIngredient> ingredients, String comment) {
		super();
		this.name = name;
		this.type = type;
		this.ingredients = ingredients;
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<DishIngredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<DishIngredient> ingredients) {
		this.ingredients = ingredients;
	}

	public String getComment() {
		return comment != null ? comment : "";
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public boolean isGaveOut() {
		return givenOut;
	}

	public void setGivenOut(boolean givenOut) {
		this.givenOut = givenOut;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ingredients, name, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dish other = (Dish) obj;
		return Objects.equals(ingredients, other.ingredients) && Objects.equals(name, other.name)
				&& Objects.equals(type, other.type);
	}

	@Override
	public String toString() {
		return "Dish [id=" + id + ", name=" + name + ", type=" + type + ", ingredients=" + ingredients + ", comment="
				+ comment + "]";
	}

}