package org.pstgu.WarehouseOrganizer.InfoModel;

public interface RepositoryManager {

	public OrderRepository getOrderRepository();

	public DishRepository getDishRepository();

	public MenuRepository getMenuRepository();

	public FoodRepository getFoodRepository();

	public ProductRepository getProductRepository();

}
