package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Order {
	private int id = -1;
	private LocalDate creationDate;
	private Provider provider;
	private LocalDate sendingDate;
	private LocalDate receiveDate;
	private String comment;
	private List<OrdProduct> products;

	public Order(LocalDate creationDate, Provider provider, String comment, List<OrdProduct> products) {
		super();
		this.creationDate = creationDate;
		this.provider = provider;
		this.comment = comment;
		this.products = products;
	}

	public Order(LocalDate creationDate, LocalDate receiveDate, Provider provider, String comment,
			List<OrdProduct> products) {
		super();
		this.creationDate = creationDate;
		this.receiveDate = receiveDate;
		this.provider = provider;
		this.comment = comment;
		this.products = products;
	}

	public Order(int id, LocalDate creationDate, Provider provider, LocalDate sendingDate, LocalDate receiveDate,
			String comment, List<OrdProduct> products) {
		super();
		this.id = id;
		this.creationDate = creationDate;
		this.provider = provider;
		this.sendingDate = sendingDate;
		this.receiveDate = receiveDate;
		this.comment = comment;
		this.products = products;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	public Provider getProvider() {
		if (provider != null)
			return provider;
		else
			return new Provider();
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public LocalDate getSendingDate() {
		return sendingDate;
	}

	public void setSendingDate(LocalDate sendingDate) {
		this.sendingDate = sendingDate;
	}

	public LocalDate getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(LocalDate receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<OrdProduct> getProducts() {
		if (products != null)
			return products;
		else
			return new ArrayList<>();
	}

	public void setProducts(List<OrdProduct> products) {
		this.products = products;
	}

	@Override
	public int hashCode() {
		return Objects.hash(comment, creationDate, products, provider, receiveDate, sendingDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		return Objects.equals(creationDate, other.creationDate) && Objects.equals(provider, other.provider)
				&& Objects.equals(comment, other.comment) && Objects.equals(products, other.products);
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", creationDate=" + creationDate + ", provider=" + provider + ", sendingDate="
				+ sendingDate + ", receiveDate=" + receiveDate + ", comment=" + comment + ", products=" + products
				+ "]";
	}

}
