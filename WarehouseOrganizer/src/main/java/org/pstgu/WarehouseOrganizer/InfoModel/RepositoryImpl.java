package org.pstgu.WarehouseOrganizer.InfoModel;

public class RepositoryImpl {
	// Table name definitions:
	protected static final String TBL_PRODUCTS = "Products";
	protected static final String TBL_FOOD_IN_STOCK = "FoodInStock";
	protected static final String TBL_ORDERS = "Orders";
	protected static final String TBL_PROVIDER = "Provider";
	protected static final String TBL_MENU = "Menu";
	protected static final String TBL_DISH = "Dish";
	protected static final String TBL_DISH_INGREDIENTS = "DishIngredient";
	protected static final String TBL_DISH_TYPES = "DishType";
	protected static final String TBL_ORDER_FILL = "OrderFill";
	protected static final String TBL_PROVIDER_SUPPLIED_PRODUCTS = "SuppliedProducts";
	protected static final String TBL_PROVIDER_DELIVERY_DAYS = "DeliveryDays";

	// Tables fields definitions:
	protected static final String F_ID = "id";
	protected static final String F_PRODUCT_ID = "productId";
	protected static final String F_ORDER_ID = "orderId";
	protected static final String F_PROVIDER_ID = "providerId";
	protected static final String F_NAME = "name";
	protected static final String F_PHONE = "phone";
	protected static final String F_MEASURE_UNIT = "measureUnit";
	protected static final String F_NUMBER_OF = "numberOf";
	protected static final String F_EXPECTED_NUMBER_OF = "expectedNumberOf";
	protected static final String F_ACTUAL_NUMBER_OF = "actualNumberOf";
	protected static final String F_LOCATION = "location";
	protected static final String F_COMMENT = "comment";
	protected static final String F_TYPE = "type";
	protected static final String F_DISH_NAME = "dishName";
	protected static final String F_DISH_ID = "dishId";
	protected static final String F_DISH_TYPE_ID = "dishTypeId";
	protected static final String F_GIVEN_OUT = "givenOut";
	protected static final String F_DATE = "date";
	protected static final String F_CREATION_DATE = "creationDate";
	protected static final String F_SENDING_DATE = "sendingDate";
	protected static final String F_RECEIVE_DATE = "receiveDate";
	protected static final String F_COOKING_TYPE = "cookingType";
	protected static final String F_DAY_OF_WEEK = "dayOfWeek";
	protected static final String F_PREFERRED_TIME = "preferredTime";

}
