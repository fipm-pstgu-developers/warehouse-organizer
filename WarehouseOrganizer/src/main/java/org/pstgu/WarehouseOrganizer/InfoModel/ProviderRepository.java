package org.pstgu.WarehouseOrganizer.InfoModel;

import java.util.List;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;

public interface ProviderRepository {

	public boolean addProvider(Provider provider);

	public List<Provider> getAllProviders();

	public Provider deleteProvider(int id) throws NotFoundException;

	/**
	 * @param key int id or String name 
	 * @throws NotFoundException
	 */
	public Provider getProvider(Object key) throws NotFoundException;

	public boolean updateProvider(Provider provider);

}
