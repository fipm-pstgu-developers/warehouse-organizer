package org.pstgu.WarehouseOrganizer.InfoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;

public class ProductRepositoryImpl extends RepositoryImpl implements ProductRepository {
	private Logger log = Logger.getLogger(getClass().getName());
	private Connection connection;

	ProductRepositoryImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public boolean add(Product product) {
		try {
			String sql = "INSERT INTO " + TBL_PRODUCTS + " (" + F_NAME + ", " + F_MEASURE_UNIT + " ) VALUES (?, ?);";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, product.getName());
			preparedStatement.setString(2, product.getMeasureUnit());
			int rows = preparedStatement.executeUpdate();
			log.log(Level.INFO,
					String.format("had been ADDED product {%s, %s}", product.getName(), product.getMeasureUnit()));
			return true;
		} catch (SQLException e) {
			log.log(Level.INFO, String.format("product {%s, %s} have been already exists", product.getName(),
					product.getMeasureUnit()));
			return false;
		}
	}

	@Override
	public Product get(Object key) throws NotFoundException {
		if (key.getClass() == String.class) {
			return get((String) key);
		} else if (key.getClass() == Integer.class) {
			return get((int) key);
		}
		throw new NotFoundException("key type not found");
	}

	public Product get(String name) throws NotFoundException {
		try {
			String sql = "SELECT * FROM " + TBL_PRODUCTS + " WHERE " + F_NAME + " = ?;";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, name);
			ResultSet r = preparedStatement.executeQuery();
			if (r.next() == false)
				throw new NotFoundException("Product with name '" + name + "' not found.");
			return new Product(r.getInt(F_ID), r.getString(F_NAME), r.getString(F_MEASURE_UNIT));
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	public Product get(int id) throws NotFoundException {
		try {
			String sql = "SELECT * FROM " + TBL_PRODUCTS + " WHERE " + F_ID + " = ?;";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			ResultSet r = preparedStatement.executeQuery();
			if (r.next() == false)
				throw new NotFoundException("Product with id '" + id + "' not found.");
			return new Product(id, r.getString(F_NAME), r.getString(F_MEASURE_UNIT));
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	@Override
	public List<Product> getAll() {
		List<Product> list = new ArrayList<>();
		try {
			String sql = "SELECT * FROM " + TBL_PRODUCTS + ";";
			Statement statement = connection.createStatement();
			ResultSet execResult = statement.executeQuery(sql);
			while (execResult.next()) {
				int id = execResult.getInt(F_ID);
				String name = execResult.getString(F_NAME);
				String measureUnit = execResult.getString(F_MEASURE_UNIT);

				Product p = new Product(id, name, measureUnit);

				list.add(p);
			}
			log.log(Level.INFO, String.format("had been GOT %d product(s)", list.size()));
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return list;
	}

	@Override
	public Product delete(Object key) throws NotFoundException {
		if (key.getClass() != String.class) {
			throw new NotFoundException("key type not found");
		}

		String name = (String) key;
		try {
			Product p = get(name);
			String sql = "DELETE FROM " + TBL_PRODUCTS + " WHERE " + F_NAME + " = ?;";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, name);
			int rows = preparedStatement.executeUpdate();
			log.log(Level.INFO, String.format("had been DELETED %d product %s", rows, p));
			return p;
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	@Override
	public boolean update(Product product) {
		try {
			String sql = "UPDATE " + TBL_PRODUCTS + " SET " + F_NAME + " = ?, " + F_MEASURE_UNIT + "  = ? WHERE " + F_ID
					+ " = ?;";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, product.getName());
			preparedStatement.setString(2, product.getMeasureUnit());
			preparedStatement.setInt(3, product.getId());
			int rows = preparedStatement.executeUpdate();
			if (rows == 0)
				throw new NotFoundException();
			log.log(Level.INFO, String.format("had been UPDATED %d product %s", rows, product));
			return true;
		} catch (NotFoundException nfe) {
			log.log(Level.WARNING, "Product with id = " + product.getId() + " not found.");
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return false;
	}

	@Override
	public List<Product> search(String byName) {
		List<Product> listProduct = new ArrayList<>();

		try {
			String sql = "SELECT * FROM " + TBL_PRODUCTS + " WHERE " + F_NAME + " LIKE '%"+byName+"%';";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);

			ResultSet pSearch = preparedStatement.executeQuery();

			while (pSearch.next()) {
				Product product = new Product(pSearch.getInt(F_ID), pSearch.getString(F_NAME),
						pSearch.getString(F_MEASURE_UNIT));
				listProduct.add(product);
			}
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}

		return listProduct;
	}

}
