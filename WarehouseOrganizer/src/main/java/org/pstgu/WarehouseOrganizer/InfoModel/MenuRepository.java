package org.pstgu.WarehouseOrganizer.InfoModel;

import java.time.LocalDate;
import java.util.Set;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Menu;

public interface MenuRepository extends CRUDRepository<Menu> {

	/**
	 * TODO @theonefeo
	 * Возвращает все продукты с их количествами из меню на день
	 *
	 * @param day - для получения меню на указанный день
	 * @return
	 */
	public Set<DishIngredient> getAllProductsFromMenuFor(LocalDate day);

	public boolean update(Dish dish);

	public boolean addDishIngredient(DishIngredient dishIngredient);
	
	public DishIngredient deleteDishIngredient(Object key) throws NotFoundException;
	
	public DishIngredient getDishIngredient(Object key) throws NotFoundException;
	
	public boolean updateDishIngredient(DishIngredient ingredient);
}
