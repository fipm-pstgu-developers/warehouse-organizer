package org.pstgu.WarehouseOrganizer.InfoModel.Data;

public abstract class Task {
	public enum TaskType {
		GIVE_OUT_FOOD, BREAD_ORDERS, DAILY_ORDERS
	}

	private int id;
	private String name;
	private String summary;
	private TaskType type;
	private boolean completed = false;

	public abstract void run();

	public Task(String name) {
		super();
		this.name = name;
	}

	public Task(String name, String summary, TaskType type) {
		this.name = name;
		this.summary = summary;
		this.type = type;
	}

	public Task(String name, String summary, TaskType type, boolean completed) {
		this(name, summary, type);
		this.completed = completed;
	}

	public Task(int id, String name, String summary, TaskType type) {
		this(name, summary, type);
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", name=" + name + ", summary=" + summary + ", type=" + type + ", completed="
				+ completed + "]";
	}

	public TaskType getType() {
		return type;
	}

}
