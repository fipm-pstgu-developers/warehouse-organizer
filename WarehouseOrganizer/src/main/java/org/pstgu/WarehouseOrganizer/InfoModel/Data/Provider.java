package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import javafx.util.Pair;

public class Provider {
	private int id = -1;
	private String name;
	private String phone;
	private List<Product> suppliedProducts;
	private Set<Pair<DayOfWeek, LocalTime>> deliveryDays;

	private Locale locale = new Locale("ru");

	public Provider() {
		suppliedProducts = new ArrayList<>();
		deliveryDays = new HashSet<>();
	}

	public Provider(String name, String phone, List<Product> suppliedProducts,
			Set<Pair<DayOfWeek, LocalTime>> deliveryDays) {
		this.name = name;
		this.phone = phone;
		this.suppliedProducts = suppliedProducts;
		this.deliveryDays = deliveryDays;
	}

	public Provider(int id, String name, String phone, List<Product> suppliedProducts,
			Set<Pair<DayOfWeek, LocalTime>> deliveryDays) {
		this(name, phone, suppliedProducts, deliveryDays);
		this.id = id;
	}

	/**
	 * TODO @theonefeo
	 *
	 * use deliveryDays set
	 *
	 * @param from date
	 * @param to   date
	 * @return list of dates with times
	 */
	public List<LocalDateTime> getProviderArrivalDates(LocalDate from, LocalDate to) {
		return null;
	}

	public LocalDateTime nextArrivalDateAfter(LocalDate date) {
		List<Pair<DayOfWeek, LocalTime>> days = new ArrayList<>(deliveryDays);
//		// sorting by day of week
//		Collections.sort(days, (d1, d2) -> d1.getKey().compareTo(d2.getKey()));
//		// sorting by time of day
//		Collections.sort(days, (t1, t2) -> t1.getValue().compareTo(t2.getValue()));

		Pair<DayOfWeek, LocalTime> nearestDay = null;
		long minDiff = Long.MAX_VALUE;
		// find min difference between List<...> days and LocalDate date.
		for (Pair<DayOfWeek, LocalTime> day : days) {
			LocalDateTime candidate = date.atStartOfDay().with(TemporalAdjusters.next(day.getKey()))
					.with(day.getValue());

			long diff = date.atStartOfDay().until(candidate, ChronoUnit.SECONDS);
			if (diff < minDiff) {
				minDiff = diff;
				nearestDay = day;
			}
		}

		return date.atStartOfDay().with(TemporalAdjusters.next(nearestDay.getKey())).with(nearestDay.getValue());
	}

	public void addSuppliedProduct(Product product) {
		suppliedProducts.add(product);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		if (name != null)
			return name;
		else
			return "не указан";
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<Product> getSuppliedProducts() {
		return suppliedProducts;
	}

	public void setSuppliedProducts(List<Product> suppliedProducts) {
		this.suppliedProducts = suppliedProducts;
	}

	public Set<Pair<DayOfWeek, LocalTime>> getDeliveryDays() {
		return deliveryDays;
	}

	public void setDeliveryDays(Set<Pair<DayOfWeek, LocalTime>> deliveryDays) {
		this.deliveryDays = deliveryDays;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@Override
	public int hashCode() {
		return Objects.hash(deliveryDays, locale, name, phone, suppliedProducts);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Provider other = (Provider) obj;
		return Objects.equals(deliveryDays, other.deliveryDays) && Objects.equals(name, other.name)
				&& Objects.equals(phone, other.phone) && Objects.equals(suppliedProducts, other.suppliedProducts);
	}

	@Override
	public String toString() {
		return "Provider [id=" + id + ", name=" + name + ", phone=" + phone + ", suppliedProducts=" + suppliedProducts
				+ ", deliveryDays=" + deliveryDays + "]";
	}

	public boolean isEmpty() {
		return name == null && phone == null;
	}

	public String getDisplayName() {
		return getName() + "\t [" + getPhone() + "]";
	}

}
