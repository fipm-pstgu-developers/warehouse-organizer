package org.pstgu.WarehouseOrganizer.InfoModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.pstgu.WarehouseOrganizer.Utils.PreferencesManager;

public class RepositoryManagerImpl implements RepositoryManager {
	private ProductRepository productRepository;
	private FoodRepository foodRepository;
	private OrderRepository orderRepository;
	private DishRepository cookbookRepository;
	private MenuRepository menuRepository;

	private Logger log = Logger.getLogger(getClass().getName());
	private Connection connection;
	
	private PreferencesManager pm = new PreferencesManager();

	// String url = "jdbc:sqlite:src/main/resources/db.sqlite";
	public RepositoryManagerImpl(String dbConnectionURL) {
		try {
			// create a connection to the database
			connection = DriverManager.getConnection(dbConnectionURL);
			log.log(Level.INFO, "Connection to DB has been established.");
		} catch (SQLException e) {
			log.log(Level.SEVERE, e.getMessage());
			closeDBConnection();
		}

		productRepository = new ProductRepositoryImpl(connection);
		foodRepository = new FoodRepositoryImpl(connection, productRepository);
		menuRepository = new MenuRepositoryImpl(connection, productRepository);
		orderRepository = new OrderRepositoryImpl(connection, productRepository);
		cookbookRepository = new DishRepositoryImpl(connection, productRepository);
		// TODO @theonefeo инициализировать все репозитории по примеру orderRepository
		// на строке выше.
	}

	public void closeDBConnection() {
		try {
			if (connection != null) {
				connection.close();
				log.log(Level.INFO, "Connection to DB has been closed.");
			}
		} catch (SQLException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}

	@Override
	public OrderRepository getOrderRepository() {
		return orderRepository;
	}

	@Override
	public DishRepository getDishRepository() {
		return cookbookRepository;
	}

	@Override
	public MenuRepository getMenuRepository() {
		return menuRepository;
	}

	@Override
	public ProductRepository getProductRepository() {
		return productRepository;
	}

	@Override
	public FoodRepository getFoodRepository() {
		return foodRepository;
	}

}
