package org.pstgu.WarehouseOrganizer.InfoModel;

import java.util.List;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;

public interface DishRepository extends CRUDRepository<Dish> {

	public List<DishIngredient> getDishIngredients(int dishId);

	public List<String> getAllTypesOfDishes();

	public List<String> getAllIngredientsForDishes();

}
