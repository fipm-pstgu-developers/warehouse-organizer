package org.pstgu.WarehouseOrganizer.InfoModel;

import java.util.List;

public interface CRUDRepository<T> {

	public boolean add(T element);

	public T get(Object key) throws NotFoundException;

	public List<T> getAll();

	public T delete(Object key) throws NotFoundException;

	public boolean update(T element);
}
