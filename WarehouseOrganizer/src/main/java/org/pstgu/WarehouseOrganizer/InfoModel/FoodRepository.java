package org.pstgu.WarehouseOrganizer.InfoModel;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.FoodProduct;

/**
 * Репозиторий продуктов на складе
 *
 * @see org.pstgu.WarehouseOrganizer.InfoModel.Data.FoodProduct
 *
 */
public interface FoodRepository extends CRUDRepository<FoodProduct> {

}
