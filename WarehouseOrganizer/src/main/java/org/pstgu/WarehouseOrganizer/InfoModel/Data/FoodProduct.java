package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import java.util.Objects;

/**
 * Продукт, который хранится на складе
 *
 */

public class FoodProduct {
	private int id = -1;
	private Product product;
	private Double numberOf;
	private String location;

	public FoodProduct() {
	}

	public FoodProduct(int id, Product product, Double numberOf, String location) {
		super();
		this.id = id;
		this.product = product;
		this.numberOf = numberOf;
		this.location = location;
	}

	public FoodProduct(Product product, Double numberOf, String location) {
		super();
		this.product = product;
		this.numberOf = numberOf;
		this.location = location;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getNumberOf() {
		return numberOf;
	}

	public void setNumberOf(Double numberOf) {
		this.numberOf = numberOf;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public int hashCode() {
		return Objects.hash(location, numberOf, product);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FoodProduct other = (FoodProduct) obj;
		return Objects.equals(location, other.location) && Objects.equals(numberOf, other.numberOf)
				&& Objects.equals(product, other.product);
	}

	@Override
	public String toString() {
		return "ProductInStock [id=" + id + ", product=" + product + ", numberOf=" + numberOf + ", location=" + location
				+ "]";
	}

}
