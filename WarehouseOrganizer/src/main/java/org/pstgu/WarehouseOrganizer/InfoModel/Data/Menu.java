package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;

public class Menu {
	public enum FoodReception {
		FIRST("Завтрак", LocalTime.parse("09:00")), SECOND("Обед", LocalTime.parse("14:00")),
		THIRD("Ужин", LocalTime.parse("18:00")), FOURTH("Предотбойник", LocalTime.parse("21:00"));

		public final String name;
		public final LocalTime time;

		FoodReception(String name, LocalTime time) {
			this.name = name;
			this.time = time;
		}

		public static FoodReception fromString(String name) {
			for (FoodReception b : FoodReception.values()) {
				if (b.name.equalsIgnoreCase(name)) {
					return b;
				}
			}
			return FIRST;
		}
	}

	private LocalDate date;
	private Map<FoodReception, List<Dish>> dishes;

	public Menu(LocalDate date, Map<FoodReception, List<Dish>> dishList) {
		super();
		this.date = date;
		this.dishes = dishList;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Map<FoodReception, List<Dish>> getDishes() {
		return dishes;
	}

	public void setDishes(Map<FoodReception, List<Dish>> dishes) {
		this.dishes = dishes;
	}

	@Override
	public String toString() {
		return "Menu [date=" + date + ", dishes=" + dishes + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, dishes);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Menu other = (Menu) obj;
		return Objects.equals(date, other.date) && Objects.equals(dishes, other.dishes);
	}

	public ObservableValue<String> getIngredientsOfDishProperty(Dish forDish) {
		return new SimpleStringProperty(forDish.getIngredients().stream()
				.map(ingredient -> ingredient.getProduct().getName()).collect(Collectors.joining("\n")).toString());
	}

	public LocalDate getActualNumberOf() {
		LocalDate actualNumberOf = null;
		// TODO Auto-generated method stub
		return actualNumberOf;
	}

}
