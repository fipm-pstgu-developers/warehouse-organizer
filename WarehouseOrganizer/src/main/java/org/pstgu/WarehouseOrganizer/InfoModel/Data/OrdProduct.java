package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import java.util.Objects;

public class OrdProduct {

	private int orderId = -1;
	private Product product;
	private Double expectedNumberOf;
	private Double actualNumberOf = 0.0;
	private String comment;

	public OrdProduct(Product product, Double expectedNumberOf, String comment) {
		this.product = product;
		this.expectedNumberOf = expectedNumberOf;
		this.comment = comment;
	}

	public OrdProduct(int orderId, Product product, Double expectedNumberOf, Double actualNumberOf, String comment) {
		this.orderId = orderId;
		this.product = product;
		this.expectedNumberOf = expectedNumberOf;
		this.actualNumberOf = actualNumberOf;
		this.comment = comment;
	}

	public Product getProduct() {
		return product;
	}

	public String getProductName() {
		return product.getName();
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getExpectedNumberOf() {
		return expectedNumberOf;
	}

	public void setExpectedNumberOf(Double expectedNumberOf) {
		this.expectedNumberOf = expectedNumberOf;
	}

	public Double getActualNumberOf() {
		return actualNumberOf;
	}

	public void setActualNumberOf(Double actualNumberOf) {
		this.actualNumberOf = actualNumberOf;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public int hashCode() {
		return Objects.hash(actualNumberOf, comment, expectedNumberOf, product);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdProduct other = (OrdProduct) obj;
		return Objects.equals(actualNumberOf, other.actualNumberOf) && Objects.equals(comment, other.comment)
				&& Objects.equals(expectedNumberOf, other.expectedNumberOf) && Objects.equals(product, other.product);
	}

	@Override
	public String toString() {
		return "OrdProduct [orderId=" + orderId + ", product=" + product + ", expectedNumberOf=" + expectedNumberOf
				+ ", actualNumberOf=" + actualNumberOf + ", comment=" + comment + "]";
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public boolean isEmpty() {
		return product == null;
	}

}
