package org.pstgu.WarehouseOrganizer.InfoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Menu;
import org.pstgu.WarehouseOrganizer.Utils.PreferencesManager;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Menu.FoodReception;

public class MenuRepositoryImpl extends RepositoryImpl implements MenuRepository {
	private Logger log = Logger.getLogger(getClass().getName());
	private Connection connection;
	private ProductRepository productRepository;
	private DBService dbService;

	private PreferencesManager prefs;

	MenuRepositoryImpl(Connection connection, ProductRepository productRepository) {
		this.connection = connection;
		this.productRepository = productRepository;

		dbService = new DBService(connection, log);

		prefs = PreferencesManager.getInstance();
	}

	@Override
	public boolean add(Menu element) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Menu> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Menu delete(Object key) throws NotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * TODO update dishes
	 */
	@Override
	public boolean update(Menu element) {
		for (Map.Entry<FoodReception, List<Dish>> menu : element.getDishes().entrySet()) {
			for (Dish dish : menu.getValue()) {
				dbService.update(TBL_MENU, new LinkedHashMap<String, Object>() {
					{
						put(F_DATE, element.getDate());
						put(F_TYPE, menu.getKey().name);
						put(F_DISH_ID, dish.getId());
					}
				}, new LinkedHashMap<String, Object>() {
					{
						put(F_GIVEN_OUT, dish.isGaveOut());
					}
				});
			}
		}
		return true;
	}

	@Override
	public Menu get(Object key) throws NotFoundException {
		if (key.getClass() == LocalDate.class) {
			return get((LocalDate) key);
		}
		throw new NotFoundException("use LocalDate for key type");
	}

	// TODO Вернуть пользователю меню на день для любой даты. Если меню на день нет,
	// сгенерировать ошибку NotFoundException
	public Menu get(LocalDate day) throws NotFoundException {

		Map<FoodReception, List<Dish>> dishList = new HashMap<>();
		try {
			for (FoodReception type : FoodReception.values()) {
				List<Dish> list = new ArrayList();
				String sql = "SELECT * FROM Menu WHERE date = ? AND type = ?;";
				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				preparedStatement.setString(1, day.toString());
				preparedStatement.setString(2, type.name);
				ResultSet execResult = preparedStatement.executeQuery();
				while (execResult.next()) {
					int dishId = execResult.getInt(F_DISH_ID);
					Dish dish = getDish(dishId);
					dish.setGivenOut("true".equals(execResult.getString(F_GIVEN_OUT)) ? true : false);
					list.add(dish);
				}
				dishList.put(type, list);
			}
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}

		return new Menu(day, dishList);
	}

	public Dish getDish(int id) throws NotFoundException {
		try {
			String sql = "SELECT Dish.dishName, DishType.type, Dish.comment FROM Dish "
					+ "JOIN DishType ON DishType.id = Dish.dishTypeId " + "WHERE Dish.id = ?;";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			ResultSet r = preparedStatement.executeQuery();
			if (r.next() == false)
				throw new NotFoundException("Dish with id '" + id + "' not found.");
			return new Dish(id, r.getString(F_DISH_NAME), r.getString(F_TYPE), getDishIngredients(id),
					r.getString(F_COMMENT));
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		throw new NotFoundException("Dish by id '" + id + "' not found.");
	}

	private List<DishIngredient> getDishIngredients(int dishId) {
		List<DishIngredient> ingredients = new ArrayList<>();
		try {
			String sql = "SELECT * FROM " + TBL_DISH_INGREDIENTS + " WHERE dishId = ?;";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, dishId);
			ResultSet r = preparedStatement.executeQuery();
			while (r.next()) {
				try {
					double expectedNumOf = r.getDouble(F_EXPECTED_NUMBER_OF) * prefs.getQuantityMultiplier();
					ingredients.add(new DishIngredient(r.getInt(F_ID), productRepository.get(r.getInt(F_PRODUCT_ID)),
							expectedNumOf, r.getDouble(F_ACTUAL_NUMBER_OF), r.getString(F_COOKING_TYPE)));
				} catch (NotFoundException e) {
				}
			}
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return ingredients;
	}

	@Override
	public Set<DishIngredient> getAllProductsFromMenuFor(LocalDate day) {
		Set<DishIngredient> products = new HashSet<>();
		try {
			String sql = "SELECT * FROM " + TBL_MENU + " WHERE " + F_DATE + " = ?;";

			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, day.toString());
			ResultSet r = preparedStatement.executeQuery();
			while (r.next()) {
				int dishId = r.getInt(F_DISH_ID);
				products.addAll(getDishIngredients(dishId));
			}
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return products;
	}

	@Override
	public boolean addDishIngredient(DishIngredient dishIngredient) {
		productRepository.update(dishIngredient.getProduct());
		return dbService.insert(TBL_DISH_INGREDIENTS, new LinkedHashMap<String, Object>() {
			{
				put(F_DISH_ID, dishIngredient.getDishId());
				put(F_PRODUCT_ID, dishIngredient.getProduct().getId());
				put(F_EXPECTED_NUMBER_OF, dishIngredient.getExpectedNumberOf() / prefs.getQuantityMultiplier());
				put(F_ACTUAL_NUMBER_OF, dishIngredient.getActualNumberOf());
				put(F_COOKING_TYPE, dishIngredient.getComment());
			}
		}) > -1;
	}

	@Override
	public boolean update(Dish dish) {
		dbService.delete(TBL_DISH_INGREDIENTS, new LinkedHashMap<String, Object>() {
			{
				put(F_DISH_ID, dish.getId());
			}
		});

		for (DishIngredient dIngr : dish.getIngredients()) {
			dIngr.setDishId(dish.getId());
			addDishIngredient(dIngr);

		}

		////

		return dbService.update(TBL_DISH, new LinkedHashMap<String, Object>() {
			{
				put(F_ID, dish.getId());
				// put(F_PRODUCT_ID, dish.getId());
			}
		}, new LinkedHashMap<String, Object>() {
			{

				put(F_DISH_NAME, dish.getName());
				put(F_COMMENT, dish.getComment());
			}
		});
	}

	@Override
	public boolean updateDishIngredient(DishIngredient dishIngredient) {
		return dbService.update(TBL_DISH_INGREDIENTS, new LinkedHashMap<String, Object>() {
			{
				put(F_ID, dishIngredient.getId());
			}
		}, new LinkedHashMap<String, Object>() {
			{
				put(F_PRODUCT_ID, dishIngredient.getProduct().getId());
				put(F_EXPECTED_NUMBER_OF, dishIngredient.getExpectedNumberOf() / prefs.getQuantityMultiplier());
				put(F_ACTUAL_NUMBER_OF, dishIngredient.getActualNumberOf());
				put(F_COOKING_TYPE, dishIngredient.getComment());
			}
		});
	}

	public DishIngredient getDishIngredient(Object key) throws NotFoundException {
		if (key.getClass() != Integer.class) {
			throw new NotFoundException("key type not found");
		}

		Integer id = (Integer) key;

		try {
			ResultSet r = dbService.select(Arrays.asList("*"), TBL_DISH_INGREDIENTS,
					new LinkedHashMap<String, Object>() {
						{
							put(F_ID, id);
						}
					});
			if (r.next() == false)
				throw new NotFoundException(String.format("DishIngredient with id=%s not found.", id));

			return new DishIngredient(r.getInt(F_ID), productRepository.get(r.getInt(F_PRODUCT_ID)),
					r.getDouble(F_EXPECTED_NUMBER_OF), r.getDouble(F_ACTUAL_NUMBER_OF), r.getString(F_COOKING_TYPE));
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
	}

	@Override
	public DishIngredient deleteDishIngredient(Object key) throws NotFoundException {
		final int id;
		if (key.getClass() == DishIngredient.class) {
			DishIngredient dIngr = (DishIngredient) key;
			id = dIngr.getId();
		} else if (key.getClass() == Integer.class) {
			id = (Integer) key;
		} else {
			throw new NotFoundException("key type not found");
		}

		DishIngredient out = getDishIngredient(id);

		if (dbService.delete(TBL_DISH_INGREDIENTS, new LinkedHashMap<String, Object>() {
			{
				put(F_ID, id);
			}
		}))
			return out;
		else
			throw new NotFoundException();

	}

}
