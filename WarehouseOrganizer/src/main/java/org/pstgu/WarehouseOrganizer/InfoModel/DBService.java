package org.pstgu.WarehouseOrganizer.InfoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBService {
	private Connection conn;
	private Logger log;

	public DBService(Connection conn, Logger log) {
		this.conn = conn;
		this.log = log;
	}

	public ResultSet select(List<String> columns, String tableName, LinkedHashMap<String, Object> conditions)
			throws Exception {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(String.join(", ", columns));

			sql.append(" FROM ").append(tableName);

			if (conditions != null) {
				sql.append(" WHERE ");
				sql.append(String.join(" = ? AND ", conditions.keySet()));
				sql.append(" = ?;");
			} else {
				sql.append(";");
			}

			PreparedStatement prepSt = getFor(sql.toString(), conditions);
			ResultSet r = prepSt.executeQuery();
			return r;
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw e;
		}
	}

	public boolean delete(String tableName, LinkedHashMap<String, Object> conditions) {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM ").append(tableName).append(" WHERE ");

			sql.append(String.join(" = ? AND ", conditions.keySet())).append(" = ?;");

			PreparedStatement prepSt = getFor(sql.toString(), conditions);
			int rows = prepSt.executeUpdate();
			log.log(Level.INFO, String.format("had been DELETED %d element(s) %s", rows, conditions));
			return true;
		} catch (SQLException e) {
			log.log(Level.INFO, String.format("the %s element did not exist in DB", conditions));
			return false;
		}
	}

	public int insert(String tableName, LinkedHashMap<String, Object> colValues) {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO ").append(tableName).append(" (");

			sql.append(String.join(", ", colValues.keySet()));

			sql.append(") VALUES (");
			for (int i = 0; i < colValues.size() - 1; i++) {
				sql.append("?, ");
			}
			sql.append("?);");

			PreparedStatement prepSt = getFor(sql.toString(), colValues);
			prepSt.executeUpdate();
			ResultSet keySet = prepSt.getGeneratedKeys();

			log.log(Level.INFO, String.format("had been ADDED element in table %s", tableName));
			return keySet.getInt(1);
		} catch (SQLException e) {
			log.log(Level.INFO,
					String.format("the %s element could not be added to the table %s", colValues, tableName));
			return -1;
		}
	}

	public boolean update(String tableName, LinkedHashMap<String, Object> conditions,
			LinkedHashMap<String, Object> newValues) {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE ").append(tableName).append(" SET ");
			sql.append(String.join(" = ?, ", newValues.keySet())).append(" = ? WHERE ");
			sql.append(String.join(" = ? AND ", conditions.keySet())).append(" = ?;");

			newValues.putAll(conditions);// for the correct operation of the method getFor
			PreparedStatement prepSt = getFor(sql.toString(), newValues);
			int rows = prepSt.executeUpdate();
			if (rows == 0)
				throw new NotFoundException();
			log.log(Level.INFO, String.format("had been UPDATED %d element(s) in table %s", rows, tableName));
			return true;
		} catch (NotFoundException ne) {
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return false;
	}

	private PreparedStatement getFor(String sql, LinkedHashMap<String, Object> paramsToInsert) throws SQLException {
//		log.log(Level.INFO, sql); // TODO remove this line
		PreparedStatement prepSt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		if (paramsToInsert != null) {
			int i = 1;
			for (Map.Entry<String, Object> entry : paramsToInsert.entrySet()) {
				Object value = entry.getValue();
				if (value instanceof Integer) {
					prepSt.setInt(i, (int) value);
				} else if (value instanceof Double) {
					prepSt.setDouble(i, (double) value);
				} else {
					prepSt.setString(i, value != null ? value.toString() : "");
				}

				i++;
			}
		}
		return prepSt;
	}

}
