package org.pstgu.WarehouseOrganizer.InfoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;

import javafx.util.Pair;

public class OrderRepositoryImpl extends RepositoryImpl implements OrderRepository {
	private Logger log = Logger.getLogger(getClass().getName());
	private Connection conn;
	private DBService dbService;
	ProductRepository productRepository;

	OrderRepositoryImpl(Connection connection, ProductRepository productRepository) {
		this.conn = connection;
		this.productRepository = productRepository;

		dbService = new DBService(conn, log);
	}

	@Override
	public Order get(Object key) throws NotFoundException {
		if (key.getClass() != Integer.class) {
			throw new NotFoundException("key type not found");
		}

		int id = (Integer) key;
		try {
			String idStr = Integer.toString(id);
			PreparedStatement prepSt = conn.prepareStatement("SELECT * FROM Orders WHERE id == ?");
			prepSt.setString(1, idStr);
			ResultSet resultSet = prepSt.executeQuery();

			if (resultSet.next() == true) {

				LocalDate creationDate = LocalDate.parse(resultSet.getString(2));
				int providerId = resultSet.getInt(3);
				String sendingDateStr = resultSet.getString(4);
				LocalDate sendingDate = sendingDateStr.equals("") ? null : LocalDate.parse(sendingDateStr);
				String receiveDateStr = resultSet.getString(5);
				LocalDate receiveDate = receiveDateStr.equals("") ? null : LocalDate.parse(receiveDateStr);
				String comment = resultSet.getString(6);
				List<OrdProduct> productList = getAllProducts(id);

				Provider provider = getProvider(providerId);

				Order order = new Order(id, creationDate, provider, sendingDate, receiveDate, comment, productList);
				order.setId(id);
				return order;
			}
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}

		throw new NotFoundException();
	}

	@Override
	public boolean add(Order order) {
		int orderId = dbService.insert(TBL_ORDERS, new LinkedHashMap<String, Object>() {
			{
				put(F_CREATION_DATE, order.getCreationDate());
				put(F_PROVIDER_ID, order.getProvider() != null ? order.getProvider().getId() : "");
				put(F_SENDING_DATE, order.getSendingDate() != null ? order.getSendingDate().toString() : "");
				put(F_RECEIVE_DATE, order.getReceiveDate() != null ? order.getReceiveDate().toString() : "");
				put(F_COMMENT, order.getComment());
			}
		});

		if (order.getProvider() != null) {
			addProvider(order.getProvider());
		}

		for (OrdProduct ordProduct : order.getProducts()) {
			if (ordProduct.getProduct() != null) {
				ordProduct.setOrderId(orderId);
				addOrdProduct(ordProduct);
			}
		}
		order.setId(orderId);

		return true;
	}

	@Override
	public List<Order> getAll() {
		return getAll(order -> true);
	}

	@Override
	public List<Order> getAll(Predicate<Order> filter) {
		List<Order> list = new ArrayList<>();
		try {
			PreparedStatement prepSt = conn
					.prepareStatement("SELECT * FROM " + TBL_ORDERS + " ORDER BY " + F_CREATION_DATE + " ASC;");
			ResultSet resultSet = prepSt.executeQuery();

			while (resultSet.next()) {
				int orderId = resultSet.getInt(F_ID);
				LocalDate creationDate = LocalDate.parse(resultSet.getString(F_CREATION_DATE));
				int providerId = resultSet.getInt(F_PROVIDER_ID);
				String sendingDateStr = resultSet.getString(F_SENDING_DATE);
				LocalDate sendingDate = sendingDateStr.equals("") ? null : LocalDate.parse(sendingDateStr);
				String receiveDateStr = resultSet.getString(F_RECEIVE_DATE);
				LocalDate receiveDate = receiveDateStr.equals("") ? null : LocalDate.parse(receiveDateStr);
				String comment = resultSet.getString(F_COMMENT);

				Provider provider = null;
				try {
					provider = getProvider(providerId);
				} catch (NotFoundException p) {
					p.printStackTrace();
					log.log(Level.WARNING, p.getMessage());
				}
				List<OrdProduct> productList = getAllProducts(orderId);
				Order o = new Order(orderId, creationDate, provider, sendingDate, receiveDate, comment, productList);
				if (filter.test(o) == true)
					list.add(o);
			}

		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		log.log(Level.INFO, String.format("had been GOT %d orders(s)", list.size()));
		return list;
	}

	@Override
	public Order delete(Object key) throws NotFoundException {
		if (key.getClass() == Integer.class) {
			return delete((int) key);
		}
		throw new NotFoundException("key type not found");
	}

	public Order delete(int id) throws NotFoundException {
		try {
			Order o = get(id);
			String sql4 = "DELETE FROM Orders WHERE id = ?;";
			PreparedStatement prepSt = conn.prepareStatement(sql4);
			prepSt.setInt(1, id);
			int rows = prepSt.executeUpdate();
			log.log(Level.INFO, String.format("had been DELETED %d order %s", rows, o));
			return o;
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	/**
	 * TODO delete supplied products and delivery days
	 */
	@Override
	public Provider deleteProvider(int id) throws NotFoundException {
		try {
			Provider p = getProvider(id);
			String sqlP = "DELETE FROM Provider WHERE id = ?;";
			PreparedStatement prepSt = conn.prepareStatement(sqlP);
			prepSt.setInt(1, id);
			int rows = prepSt.executeUpdate();
			log.log(Level.INFO, String.format("had been DELETED %d provider %s", rows, p));
			return p;
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	@Override
	public Provider getProvider(Object key) throws NotFoundException {
		if (key.getClass() == String.class) {
			return getProvider((String) key);
		} else if (key.getClass() == Integer.class) {
			return getProvider((int) key);
		}
		throw new NotFoundException("key type not found");
	}

	public Provider getProvider(int id) throws NotFoundException {
		try {
			ResultSet r = dbService.select(Arrays.asList("*"), TBL_PROVIDER, new LinkedHashMap<String, Object>() {
				{
					put(F_ID, id);
				}
			});
			if (r.next() == false) {
				throw new NotFoundException("Provider with id = " + id + " not found");
			}
			return new Provider(id, r.getString(F_NAME), r.getString(F_PHONE), getSuppliedProducts(id),
					getDediveryDays(id));
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.WARNING, e.getMessage());
		}
		throw new NotFoundException("Provider with id = " + id + " not found");
	}

	public Provider getProvider(String providerName) throws NotFoundException {
		try {
			ResultSet r = dbService.select(Arrays.asList("*"), TBL_PROVIDER, new LinkedHashMap<String, Object>() {
				{
					put(F_NAME, providerName);
				}
			});
			if (r.next() == false) {
				throw new NotFoundException("Provider with name = " + providerName + " not found");
			}
			int id = r.getInt(F_ID);
			return new Provider(id, providerName, r.getString(F_PHONE), getSuppliedProducts(id), getDediveryDays(id));
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.WARNING, e.getMessage());
		}
		throw new NotFoundException("Provider with name = " + providerName + " not found");
	}

	private List<Provider> getAllProviders_stub() {
		HashSet<Pair<DayOfWeek, LocalTime>> deliveryDays = new HashSet<>();
		deliveryDays.add(new Pair<DayOfWeek, LocalTime>(DayOfWeek.MONDAY, LocalTime.parse("08:00:00")));
		deliveryDays.add(new Pair<DayOfWeek, LocalTime>(DayOfWeek.TUESDAY, LocalTime.parse("10:00:00")));
		deliveryDays.add(new Pair<DayOfWeek, LocalTime>(DayOfWeek.WEDNESDAY, LocalTime.parse("10:00:00")));
		deliveryDays.add(new Pair<DayOfWeek, LocalTime>(DayOfWeek.THURSDAY, LocalTime.parse("08:00:00")));
		deliveryDays.add(new Pair<DayOfWeek, LocalTime>(DayOfWeek.FRIDAY, LocalTime.parse("08:00:00")));
		deliveryDays.add(new Pair<DayOfWeek, LocalTime>(DayOfWeek.SATURDAY, LocalTime.parse("10:00:00")));

		HashSet<Pair<DayOfWeek, LocalTime>> deliveryDays2 = new HashSet<>();
		deliveryDays2.add(new Pair<DayOfWeek, LocalTime>(DayOfWeek.TUESDAY, LocalTime.parse("11:30:00")));

		ArrayList<Provider> providers = new ArrayList<>();
		providers.add(new Provider(1, "Хлеб", "791533333333",
				Arrays.asList(new Product("Хлеб-ржаной", "шт"), new Product("Хлеб-батон", "шт")), deliveryDays));
		providers.add(new Provider(4, "Овощи-фрукты", "791533333333",
				Arrays.asList(new Product("Огурцы", "кг"), new Product("Персики", "кг")), deliveryDays2));
		providers.add(new Provider(5, "Молочка", "791533333333",
				Arrays.asList(new Product("Молоко", "л"), new Product("Сыр", "кг")), deliveryDays));
		return providers;
	}

	@Override
	public List<Provider> getAllProviders() {
		List<Provider> list = new ArrayList<>();

		try {
			ResultSet r = dbService.select(Arrays.asList("*"), TBL_PROVIDER, null);
			while (r.next()) {
				int providerId = r.getInt(F_ID);
				list.add(new Provider(providerId, r.getString(F_NAME), r.getString(F_PHONE),
						getSuppliedProducts(providerId), getDediveryDays(providerId)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.WARNING, e.getMessage());
		}
		log.log(Level.INFO, String.format("had been GOT %d provider(s)", list.size()));
		return list;
	}

	private Set<Pair<DayOfWeek, LocalTime>> getDediveryDays(int providerId) {
		Set<Pair<DayOfWeek, LocalTime>> set = new HashSet<>();
		try {
			ResultSet r = dbService.select(Arrays.asList("*"), TBL_PROVIDER_DELIVERY_DAYS,
					new LinkedHashMap<String, Object>() {
						{
							put(F_PROVIDER_ID, providerId);
						}
					});
			while (r.next()) {
				set.add(new Pair<DayOfWeek, LocalTime>(DayOfWeek.valueOf(r.getString(F_DAY_OF_WEEK)),
						LocalTime.parse(r.getString(F_PREFERRED_TIME))));
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.WARNING, e.getMessage());
		}
		return set;
	}

	public List<Product> getSuppliedProducts(int providerId) {
		List<Product> list = new ArrayList<Product>();
		try {
			ResultSet r = dbService.select(Arrays.asList("*"), TBL_PROVIDER_SUPPLIED_PRODUCTS,
					new LinkedHashMap<String, Object>() {
						{
							put(F_PROVIDER_ID, providerId);
						}
					});
			while (r.next()) {
				list.add(productRepository.get(r.getInt(F_PRODUCT_ID)));
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.WARNING, e.getMessage());
		}

		return list;
	}

	@Override
	public List<OrdProduct> getAllProducts(Order forOrder) {
		return getAllProducts(forOrder.getId());
	}

	List<OrdProduct> getAllProducts(int orderId) {
		List<OrdProduct> productList = new ArrayList<OrdProduct>();
		try {
			PreparedStatement prepSt2 = conn.prepareStatement("SELECT * FROM OrderFill WHERE orderId == ?");
			prepSt2.setInt(1, orderId);
			ResultSet setOfProducts = prepSt2.executeQuery();
			while (setOfProducts.next()) {
				int productId = setOfProducts.getInt("productId");
				double expectedNumberOf = setOfProducts.getDouble("expectedNumberOf");
				double actualNumberOf = setOfProducts.getDouble("actualNumberOf");
				String commentOrderFill = setOfProducts.getString("comment");

				try {
					Product product = productRepository.get(productId);
					OrdProduct productOrder = new OrdProduct(orderId, product, expectedNumberOf, actualNumberOf,
							commentOrderFill);
					productList.add(productOrder);
				} catch (NotFoundException e) {
					e.printStackTrace();
					log.log(Level.WARNING, e.getMessage());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			log.log(Level.WARNING, e.getMessage());
		}
		return productList;

	}

	@Override
	public boolean addProvider(Provider provider) {
		try {
			getProvider(provider.getName());
			return false;
		} catch (Exception e) {
		}

		int providerId = dbService.insert(TBL_PROVIDER, new LinkedHashMap<String, Object>() {
			{
				put(F_NAME, provider.getName());
				put(F_PHONE, provider.getPhone());
			}
		});
		if (providerId <= -1)
			return false;

		for (Product product : provider.getSuppliedProducts()) {
			final int productId;
			try {
				productRepository.add(product);
				productId = productRepository.get(product.getName()).getId();
			} catch (NotFoundException e) {
				e.printStackTrace();
				log.log(Level.WARNING, e.getMessage());
				continue;
			}
			dbService.insert(TBL_PROVIDER_SUPPLIED_PRODUCTS, new LinkedHashMap<String, Object>() {
				{
					put(F_PROVIDER_ID, providerId);
					put(F_PRODUCT_ID, productId);
				}
			});
		}
		for (Pair<DayOfWeek, LocalTime> day : provider.getDeliveryDays()) {
			dbService.insert(TBL_PROVIDER_DELIVERY_DAYS, new LinkedHashMap<String, Object>() {
				{
					put(F_PROVIDER_ID, providerId);
					put(F_DAY_OF_WEEK, day.getKey());
					put(F_PREFERRED_TIME, day.getValue());
				}
			});
		}

		return true;
	}

	/**
	 * TODO replace stub
	 */
	@Override
	public List<Provider> getProvidersIn(LocalDate day) {
		Set<Integer> providers = new HashSet<>();
		try {
			ResultSet r = dbService.select(Arrays.asList("*"), TBL_PROVIDER_DELIVERY_DAYS,
					new LinkedHashMap<String, Object>() {
						{
							put(F_DAY_OF_WEEK, day.getDayOfWeek());
						}
					});
			while (r.next()) {
				providers.add(r.getInt(F_PROVIDER_ID));
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.WARNING, e.getMessage());
		}

		return providers.stream().map(providerId -> {
			try {
				return getProvider(providerId);
			} catch (NotFoundException e) {
				e.printStackTrace();
				log.log(Level.WARNING, e.getMessage());
			}
			return null;
		}).filter(e -> e != null).collect(Collectors.toList());
	}

	/**
	 * TODO удалить все продукты из таблиц БД, которые находились в заказе, и
	 * добавить их заново
	 */
	@Override
	public boolean update(Order order) {
		try {
			String sql = "UPDATE " + TBL_ORDERS
					+ " SET creationDate = ?, providerId = ?, sendingDate = ?, receiveDate = ?, comment = ? WHERE id = ?;";
			PreparedStatement prepSt = conn.prepareStatement(sql);
			prepSt.setString(1, order.getCreationDate().toString());
			prepSt.setInt(2, order.getProvider().getId());
			prepSt.setString(3, order.getSendingDate() != null ? order.getSendingDate().toString() : "");
			if (order.getReceiveDate() != null)
				prepSt.setString(4, order.getReceiveDate().toString());
			else
				prepSt.setString(4, "");
			prepSt.setString(5, order.getComment());
			prepSt.setInt(6, order.getId());

			int rows = prepSt.executeUpdate();
			if (rows == 0)
				throw new NotFoundException();

			for (OrdProduct p : order.getProducts()) {
				try {
					addOrdProduct(deleteOrdProduct(p));
				} catch (NotFoundException e) {
					addOrdProduct(p);
				}
			}

			log.log(Level.INFO, String.format("had been UPDATED %d orders %s", rows, order));
			return true;
		} catch (NotFoundException eNF) {
			log.log(Level.WARNING, "Order with id = " + order.getId() + " not found.");
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return false;
	}

	/**
	 * TODO update suppliedProducts and deliveryDays
	 */
	@Override
	public boolean updateProvider(Provider provider) {
		try {
			String sql = "UPDATE " + TBL_PROVIDER + " SET " + F_NAME + " = ?, " + F_PHONE + "  = ? WHERE " + F_ID
					+ " = ?;";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setString(1, provider.getName());
			preparedStatement.setString(2, provider.getPhone());
			preparedStatement.setInt(3, provider.getId());
			int rows = preparedStatement.executeUpdate();
			if (rows == 0)
				throw new NotFoundException();

			// TODO update suppliedProducts and deliveryDays from provider

			log.log(Level.INFO, String.format("had been UPDATED %d provider %s", rows, provider));
			return true;
		} catch (NotFoundException nfe) {
			log.log(Level.WARNING, "Provider with id = " + provider.getId() + " not found.");
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return false;
	}

	@Override
	public boolean addOrdProduct(OrdProduct product) {

		return dbService.insert(TBL_ORDER_FILL, new LinkedHashMap<String, Object>() {
			{
				put(F_ORDER_ID, product.getOrderId());
				put(F_PRODUCT_ID, product.getProduct() != null ? product.getProduct().getId() : null);
				put(F_EXPECTED_NUMBER_OF, product.getExpectedNumberOf());
				put(F_ACTUAL_NUMBER_OF, product.getActualNumberOf());
				put(F_COMMENT, product.getComment());
			}
		}) > -1;

	}

	@Override
	public OrdProduct getOrdProduct(Object key) throws NotFoundException {
		if (key.getClass() != Pair.class) {
			throw new NotFoundException("key type not found");
		}

		Pair<Integer, Integer> keys = (Pair<Integer, Integer>) key;
		Integer orderId = keys.getKey();
		Integer productId = keys.getValue();

		try {
			ResultSet r = dbService.select(Arrays.asList("*"), TBL_ORDER_FILL, new LinkedHashMap<String, Object>() {
				{
					put(F_ORDER_ID, orderId);
					put(F_PRODUCT_ID, productId);
				}
			});
			if (r.next() == false)
				throw new NotFoundException(
						String.format("OrdProduct with orderId=%s and productId=%s not found.", orderId, productId));

			return new OrdProduct(r.getInt(F_ORDER_ID), productRepository.get(r.getInt(F_PRODUCT_ID)),
					r.getDouble(F_EXPECTED_NUMBER_OF), r.getDouble(F_ACTUAL_NUMBER_OF), r.getString(F_COMMENT));
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}

	}

	@Override
	public OrdProduct deleteOrdProduct(Object key) throws NotFoundException {
		if (key.getClass() != OrdProduct.class) {
			throw new NotFoundException("key type not found");
		}

		OrdProduct in = (OrdProduct) key;
		if (in.getProduct() == null) {
			throw new NotFoundException();
		}
		OrdProduct out = getOrdProduct(new Pair(in.getOrderId(), in.getProduct().getId()));

		if (dbService.delete(TBL_ORDER_FILL, new LinkedHashMap<String, Object>() {
			{
				put(F_ORDER_ID, in.getOrderId());
				put(F_PRODUCT_ID, in.getProduct().getId());
			}
		}))
			return out;
		else
			throw new NotFoundException();
	}

	@Override
	public boolean updateOrdProduct(OrdProduct product) {
		return dbService.update(TBL_ORDER_FILL, new LinkedHashMap<String, Object>() {
			{
				put(F_ORDER_ID, product.getOrderId());
				put(F_PRODUCT_ID, product.getProduct().getId());
			}
		}, new LinkedHashMap<String, Object>() {
			{
				put(F_EXPECTED_NUMBER_OF, product.getExpectedNumberOf());
				put(F_ACTUAL_NUMBER_OF, product.getActualNumberOf());
				put(F_COMMENT, product.getComment());
			}
		});
	}

	@Override
	public List<Order> search(List<Product> products) {
		Set<Integer> ordersIdSet = new HashSet<>();
		for (Product product : products) {
			try {
				String sql = "SELECT O." + F_ORDER_ID + " FROM " + TBL_ORDER_FILL + " AS O JOIN " + TBL_PRODUCTS
						+ " AS P ON O." + F_PRODUCT_ID + " = P." + F_ID + " WHERE P." + F_ID + " = ?;";
				PreparedStatement st = conn.prepareStatement(sql);
				st.setInt(1, product.getId());
				ResultSet rs = st.executeQuery();
				while (rs.next()) {
					ordersIdSet.add(rs.getInt(F_ORDER_ID));
				}
			} catch (SQLException e) {
				e.printStackTrace();
				log.log(Level.WARNING, e.getMessage());
			}
		}
		return ordersIdSet.stream().map(orderId -> {
			try {
				return get(orderId);
			} catch (NotFoundException e) {
				e.printStackTrace();
				log.log(Level.WARNING, e.getMessage());
			}
			return null;
		}).filter(e -> e != null).collect(Collectors.toList());
	}

	@Override
	public List<Order> getUnsentBreadOrders() {
		List<Product> bread = productRepository.search("леб");
		List<Order> breadOrders = search(bread).stream().filter(order -> order.getSendingDate() == null)
				.collect(Collectors.toList());
		return breadOrders;
	}

	@Override
	public List<Order> getUnsentNotBreadOrders() {
		List<Order> orders = getAll(order -> order.getSendingDate() == null);
		orders.removeAll(getUnsentBreadOrders());
		return orders;
	}

	@Override
	public List<OrdProduct> getOrdPoductsFromUnsentOrders() {
		return getAll(order -> order.getSendingDate() == null).stream().flatMap(order -> order.getProducts().stream())
				.collect(Collectors.toList());
	}

}
