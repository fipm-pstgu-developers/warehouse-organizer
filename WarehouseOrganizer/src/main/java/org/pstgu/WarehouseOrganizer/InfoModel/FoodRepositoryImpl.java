package org.pstgu.WarehouseOrganizer.InfoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.FoodProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;

public class FoodRepositoryImpl extends RepositoryImpl implements FoodRepository {
	private Logger log = Logger.getLogger(getClass().getName());
	private Connection conn;
	private ProductRepository productRepository;

	FoodRepositoryImpl(Connection connection, ProductRepository productRepository) {
		this.conn = connection;
		this.productRepository = productRepository;

	}

	@Override
	public List<FoodProduct> getAll() {
		List<FoodProduct> list = new ArrayList<>();
		try {
			String sql = "SELECT * FROM " + TBL_FOOD_IN_STOCK + ";";
			Statement statement = conn.createStatement();
			ResultSet execResult = statement.executeQuery(sql);
			while (execResult.next()) {
				int id = execResult.getInt(F_ID);
				int productId = execResult.getInt(F_PRODUCT_ID);
				double numberOf = execResult.getDouble(F_NUMBER_OF);
				String location = execResult.getString(F_LOCATION);

				FoodProduct p = new FoodProduct(id, productRepository.get(productId), numberOf, location);

				list.add(p);
			}
			log.log(Level.INFO, String.format("had been GOT %d roduct(s)InStock", list.size()));
		} catch (SQLException | NotFoundException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return list;
	}

	@Override
	public boolean add(FoodProduct productInStock) {
		try {
			String sql = "INSERT INTO " + TBL_FOOD_IN_STOCK + " (productId, numberOf, location) VALUES (?, ?, ?);";
			PreparedStatement prepSt = conn.prepareStatement(sql);
			prepSt.setInt(1, productInStock.getProduct().getId());
			prepSt.setDouble(2, productInStock.getNumberOf());
			prepSt.setString(3, (productInStock.getLocation()));
			int rows = prepSt.executeUpdate();

			productRepository.add(productInStock.getProduct());

			log.log(Level.INFO, String.format("had been ADDED %s", productInStock));
			return true;
		} catch (SQLException e) {
			log.log(Level.INFO, String.format("%s have been already exists", productInStock));
			return false;
		}
	}

	@Override
	public FoodProduct get(Object key) throws NotFoundException {
		if (key.getClass() == String.class) {
			return get((String) key);
		} else if (key.getClass() == Integer.class) {
			return get((int) key);
		} else if (key.getClass() == Product.class) {
			return get(((Product) key).getName());
		}
		throw new NotFoundException("key type not found");
	}

	public FoodProduct get(String productName) throws NotFoundException {
		try {
			Product product = productRepository.get(productName);

			PreparedStatement prepSt1 = conn
					.prepareStatement("SELECT * FROM " + TBL_FOOD_IN_STOCK + " WHERE productId == ?");
			prepSt1.setInt(1, product.getId());
			ResultSet resultInStock = prepSt1.executeQuery();

			if (resultInStock.next() == false) {
				throw new NotFoundException("FoodProduct with product name = \"" + productName + "\" not found.");
			}
			int id = resultInStock.getInt(F_ID);
			double numberOf = resultInStock.getDouble(F_NUMBER_OF);
			String location = resultInStock.getString(F_LOCATION);

			return new FoodProduct(id, product, numberOf, location);

//			throw new NotFoundException("FoodProduct with Product.name = '" + productName + "' not found.");
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	public FoodProduct get(int id) throws NotFoundException {
		try {
			PreparedStatement prepSt = conn.prepareStatement("SELECT * FROM " + TBL_FOOD_IN_STOCK + " WHERE id == ?");
			prepSt.setInt(1, id);
			ResultSet resultStock = prepSt.executeQuery();

			if (resultStock.next() == true) {
				Product product = productRepository.get(resultStock.getInt(F_PRODUCT_ID));
				double numberOf = resultStock.getDouble(F_NUMBER_OF);
				String location = resultStock.getString(F_LOCATION);

				return new FoodProduct(id, product, numberOf, location);
			}
			throw new NotFoundException("FoodProduct with id = '" + id + "' not found.");
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	@Override
	public FoodProduct delete(Object key) throws NotFoundException {
		if (key.getClass() != FoodProduct.class) {
			throw new NotFoundException("key type not found");
		}

		FoodProduct productInStock = (FoodProduct) key;
		try {
			String sql = "DELETE FROM " + TBL_FOOD_IN_STOCK + " WHERE id = ?;";
			PreparedStatement prepSt = conn.prepareStatement(sql);
			prepSt.setInt(1, productInStock.getId());
			int rows = prepSt.executeUpdate();
			log.log(Level.INFO, String.format("had been DELETED %d FoodProduct %s", rows, productInStock));
			return productInStock;
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
			throw new NotFoundException(e.getMessage());
		}
	}

	@Override
	public boolean update(FoodProduct foodProduct) {
		try {
			if (!productRepository.update(foodProduct.getProduct())) {
				return false;
			}

			String sql = "UPDATE " + TBL_FOOD_IN_STOCK + " SET productId = ?, numberOf = ?, location = ? WHERE id = ?;";
			PreparedStatement prepSt = conn.prepareStatement(sql);
			prepSt.setInt(1, foodProduct.getProduct().getId());
			prepSt.setDouble(2, foodProduct.getNumberOf());
			prepSt.setString(3, foodProduct.getLocation());
			prepSt.setInt(4, foodProduct.getId());
			prepSt.executeUpdate();

			log.log(Level.INFO, String.format("had been UPDATED FoodProduct %s", foodProduct));
			return true;
		} catch (SQLException e) {
			log.log(Level.WARNING, e.getMessage());
		}
		return false;
	}

}
