package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import java.util.Objects;

public class DishIngredient {

	private int id = -1;
	private int dishId = -1;
	private Product product;
	private Double expectedNumberOf;
	private Double actualNumberOf;
	private String comment;

	public DishIngredient(int id, Product product, Double expectedNumberOf, Double actualNumberOf, String comment) {
		super();
		this.id = id;
		this.product = product;
		this.expectedNumberOf = expectedNumberOf;
		this.actualNumberOf = actualNumberOf;
		this.comment = comment;
	}

	public DishIngredient(Product product, Double expectedNumberOf, Double actualNumberOf, String comment) {
		super();
		this.product = product;
		this.expectedNumberOf = expectedNumberOf;
		this.actualNumberOf = actualNumberOf;
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDishId() {
		return dishId;
	}

	public void setDishId(int dishId) {
		this.dishId = dishId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getExpectedNumberOf() {
		return expectedNumberOf;
	}

	public void setExpectedNumberOf(Double expectedNumberOf) {
		this.expectedNumberOf = expectedNumberOf;
	}

	public Double getActualNumberOf() {
		return actualNumberOf;
	}

	public void setActualNumberOf(Double actualNumberOf) {
		this.actualNumberOf = actualNumberOf;
	}

	public String getComment() {
		return comment != null ? comment : "";
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public int hashCode() {
		return Objects.hash(actualNumberOf, comment, expectedNumberOf, product);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DishIngredient other = (DishIngredient) obj;
		return Objects.equals(actualNumberOf, other.actualNumberOf) && Objects.equals(comment, other.comment)
				&& Objects.equals(expectedNumberOf, other.expectedNumberOf) && Objects.equals(product, other.product);
	}

	@Override
	public String toString() {
		return "DishIngredient [id=" + id + ", product=" + product + ", expectedNumberOf=" + expectedNumberOf
				+ ", actualNumberOf=" + actualNumberOf + ", comment=" + comment + "]";
	}

}
