package org.pstgu.WarehouseOrganizer.InfoModel;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;

public interface OrderRepository extends CRUDRepository<Order>, ProviderRepository {

	/**
	 * <b>Какую проблему решает данный метод?</b> <br>
	 *
	 * Получить все продукты из заказа.
	 *
	 * @param orderId - id заказа
	 * @return все продукты которые были добавлены в заказ
	 * @throws SQLException
	 */
	public List<OrdProduct> getAllProducts(Order forOrder);

	public boolean addOrdProduct(OrdProduct product);

	public OrdProduct getOrdProduct(Object key) throws NotFoundException;

	public OrdProduct deleteOrdProduct(Object key) throws NotFoundException;

	public boolean updateOrdProduct(OrdProduct product);

	default public void addAll(List<Order> orders) {
		List<Order> ordersInRepository = getAll();
		for (Order order : orders) {
			if (!ordersInRepository.contains(order)) {
				add(order);
			}
		}
	}

	/**
	 * TODO @theonefeo
	 *
	 * Получить поставщиков, которые приезжают в заданный день
	 *
	 * @param day
	 * @return
	 */
	public List<Provider> getProvidersIn(LocalDate day);

	// public List<Provider> getProvidersFor(Product forProduct);

	/**
	 * TODO @hope1107 Найти все заказы, в которых имеются продукты
	 *
	 * @param products
	 * @return list of orders that contain products
	 */
	public List<Order> search(List<Product> products);

	public List<Order> getAll(Predicate<Order> filter);

	public List<Order> getUnsentBreadOrders();

	public List<Order> getUnsentNotBreadOrders();
	
	public List<OrdProduct> getOrdPoductsFromUnsentOrders();
}
