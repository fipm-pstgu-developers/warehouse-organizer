package org.pstgu.WarehouseOrganizer.InfoModel.Data;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Menu.FoodReception;

public abstract class GiveOutTask extends Task {
	private FoodReception foodReception;

	public GiveOutTask(String name, String summary, FoodReception foodReception, boolean completed) {
		super(name, summary, TaskType.GIVE_OUT_FOOD, completed);
		this.foodReception = foodReception;
	}

	public GiveOutTask(String name, String summary, FoodReception foodReception) {
		this(name, summary, foodReception, false);
	}

	public FoodReception getFoodReception() {
		return foodReception;
	}
}
