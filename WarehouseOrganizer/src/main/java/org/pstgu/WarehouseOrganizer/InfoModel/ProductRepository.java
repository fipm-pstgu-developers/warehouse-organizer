package org.pstgu.WarehouseOrganizer.InfoModel;

import java.util.List;

import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;

public interface ProductRepository extends CRUDRepository<Product> {

	/**
	 * TODO @theonefeo Найти все продукты по части названия
	 *
	 * @param byName - part of name
	 * @return list of products whose names contain byName
	 */
	public List<Product> search(String byName);

}
