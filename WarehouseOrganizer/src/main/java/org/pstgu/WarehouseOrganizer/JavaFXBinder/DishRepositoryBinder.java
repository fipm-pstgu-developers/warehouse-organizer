package org.pstgu.WarehouseOrganizer.JavaFXBinder;

import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.pstgu.WarehouseOrganizer.InfoModel.DishRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DishRepositoryBinder {
	private Logger log = Logger.getLogger(getClass().getName());
	private ObservableList<Dish> dishes;
	private ObservableList<String> dishTypes;
	private ObservableList<String> ingredientsOfAllDishes;
	private DishRepository dishRepository;

	public DishRepositoryBinder(DishRepository dishRepository) {
		this.dishRepository = dishRepository;

		// TODO
		dishes = FXCollections.observableList(dishRepository.getAll());
		// TODO
		dishTypes = FXCollections.observableList(dishRepository.getAllTypesOfDishes());
		// TODO
		ingredientsOfAllDishes = FXCollections.observableList(dishRepository.getAllIngredientsForDishes());
	}

	public ObservableValue<String> dishNameProperty(Dish forDish) {
		return new SimpleStringProperty(forDish.getName()) {
			@Override
			public void set(String newValue) {
				forDish.setName(newValue);
			dishRepository.update(forDish);
			}
		};
	}

	public ObservableValue<String> ingredientNameProperty(DishIngredient ingredient) {
		return new SimpleStringProperty(ingredient.getProduct().getName());
	}

	public ObservableValue<String> ingredientNumberOfProperty(DishIngredient ingredient) {
		return new SimpleStringProperty(
				ingredient.getExpectedNumberOf() + " " + ingredient.getProduct().getMeasureUnit());
	}

	public ObservableValue<String> dishTypeProperty(Dish forDish) {
		return new SimpleStringProperty(forDish.getType());
	}

	public ObservableValue<String> getIngredientsOfDishProperty(Dish forDish) {
		return new SimpleStringProperty(forDish.getIngredients().stream()
				.map(ingredient -> ingredient.getProduct().getName()).collect(Collectors.joining("\n")).toString());
	}

	public ObservableList<DishIngredient> getDishIngredientsList(Dish forDish) {
		return FXCollections.observableList(forDish.getIngredients());
	}

	public ObservableList<Dish> getDishList() {
		return dishes;
	}

	public ObservableList<String> getDishTypesList() {
		return dishTypes;
	}

	public ObservableList<String> getAllDishIngredientsList() {
		return ingredientsOfAllDishes;
	}
}
