package org.pstgu.WarehouseOrganizer.JavaFXBinder;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.pstgu.WarehouseOrganizer.InfoModel.NotFoundException;
import org.pstgu.WarehouseOrganizer.InfoModel.OrderRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.ProductRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class OrderRepositoryBinder {
	private Logger log = Logger.getLogger(getClass().getName());
	private ObservableList<Order> orders;
	private ObservableList<Provider> providers;
	private ObservableList<OrdProduct> products;
	private OrderRepository orderRepository;
	private ProductRepository productRepository;

	public OrderRepositoryBinder(OrderRepository orderRepository, ProductRepository productRepository) {
		this.orderRepository = orderRepository;
		this.productRepository = productRepository;

	}

	public ObservableValue<String> providerNameProperty(Order forOrder) {
		return new SimpleStringProperty(forOrder.getProvider().getName()) {
			@Override
			public void set(String newValue) {
				forOrder.getProvider().setName(newValue);
				orderRepository.update(forOrder);
			}
		};
	}

	public ObservableList<Order> getOrders() {
		orders = FXCollections.observableList(orderRepository.getAll());
		orders.addListener(new ListChangeListener<Order>() {

			@Override
			public void onChanged(Change<? extends Order> c) {
				while (c.next()) {
					if (c.wasAdded()) {
						for (Order o : c.getAddedSubList()) {
							orderRepository.add(o);
						}
					} else if (c.wasRemoved()) {
						for (Order o : c.getRemoved()) {
							try {
								orderRepository.delete(o.getId());
							} catch (NotFoundException e) {
								log.log(Level.WARNING, e.getMessage());
							}
						}
					}

				}
			}

		});
		return orders;
	}

	public Provider getProvider(String providerName) throws NotFoundException {
		return orderRepository.getProvider(providerName);
	}

	public ObservableList<Provider> getProviders() {
		providers = FXCollections.observableList(orderRepository.getAllProviders());

		providers.addListener(new ListChangeListener<Provider>() {

			@Override
			public void onChanged(Change<? extends Provider> p) {
				while (p.next()) {
					if (p.wasAdded()) {
						for (Provider o : p.getAddedSubList()) {
							orderRepository.addProvider(o);
						}
					} else if (p.wasRemoved()) {
						for (Provider o : p.getRemoved()) {
							try {
								orderRepository.deleteProvider(o.getId());
							} catch (NotFoundException e) {
								log.log(Level.SEVERE, e.getMessage());
							}
						}
					}

				}
			}

		});

		return providers;
	}

	public ObservableValue<String> providerNameProperty(Provider provider) {
		return new SimpleStringProperty(provider.getName());
	}

	public ObservableValue<String> providerPhoneProperty(Provider provider) {
		return new SimpleStringProperty(provider.getPhone());
	}

	public ObservableValue<String> providerDaysProperty(Provider provider) {
		return new SimpleStringProperty(
				provider.getDeliveryDays().stream().sorted((p1, p2) -> p1.getKey().compareTo(p2.getKey()))
						.map(deliveryDays -> deliveryDays.getKey().getDisplayName(TextStyle.SHORT, provider.getLocale())
								+ "\t  " + deliveryDays.getValue().toString())
						.collect(Collectors.joining("\n")).toString());
		// .collect(Collectors.joining("\n")).toString());
	}

	public ObservableList<OrdProduct> getProducts(Order forOrder) {
		products = FXCollections.observableList(orderRepository.getAllProducts(forOrder));
//		products.addListener(new ListChangeListener<OrdProduct>() {
//
//			@Override
//			public void onChanged(Change<? extends OrdProduct> p) {
//				while (p.next()) {
//					if (p.wasAdded()) {
//						for (OrdProduct o : p.getAddedSubList()) {
//							o.setOrderId(forOrder.getId());
//							orderRepository.addOrdProduct(o);
//						}
//					} else if (p.wasRemoved()) {
//						for (OrdProduct o : p.getRemoved()) {
//							try {
//								orderRepository.deleteOrdProduct(0);
//							} catch (NotFoundException e) {
//								log.log(Level.WARNING, e.getMessage());
//							}
//						}
//					}
//
//				}
//			}
//
//		});

		return products;
	}

	public ObservableValue<String> creationDateProperty(Order forOrder) {
		LocalDate date = forOrder.getCreationDate();
		return new SimpleStringProperty(date == null ? "-" : date.format(DateTimeFormatter.ofPattern("dd.MM"))) {

			@Override
			public void set(String newValue) {
				forOrder.setCreationDate(stringToLocalDate(newValue));
				orderRepository.update(forOrder);
			}

		};
	}

	public ObservableValue<String> receiveDateProperty(Order forOrder) {
		LocalDate date = forOrder.getReceiveDate();
		return new SimpleStringProperty(date == null ? "-" : date.format(DateTimeFormatter.ofPattern("dd.MM"))) {

			@Override
			public void set(String newValue) {
				forOrder.setReceiveDate(stringToLocalDate(newValue));
				orderRepository.update(forOrder);
			}

		};
	}

	public StringProperty sendingDateProperty(Order forOrder) {
		LocalDate date = forOrder.getSendingDate();
		return new SimpleStringProperty(date == null ? "-" : date.format(DateTimeFormatter.ofPattern("dd.MM"))) {

			@Override
			public void set(String newValue) {
				forOrder.setSendingDate(stringToLocalDate(newValue));
				orderRepository.update(forOrder);
			}

		};
	}

	public ObservableValue<String> orderCommentProperty(Order forOrder) {
		return new SimpleStringProperty(forOrder.getComment()) {

			@Override
			public void set(String newValue) {
				forOrder.setComment(newValue);
				orderRepository.update(forOrder);
			}

		};
	}

	LocalDate stringToLocalDate(String dayAndMonth) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

		LocalDate newDate = LocalDate.parse(dayAndMonth + "." + LocalDate.now().getYear(), formatter);

		return newDate;
	}

	public ObservableValue<String> productNameProp(OrdProduct forProduct) {
		return new SimpleStringProperty(forProduct.getProduct() != null
				? forProduct.getProduct().getName() + " (" + forProduct.getProduct().getMeasureUnit() + ")"
				: null) {
			@Override
			public void set(String newValue) {
				// TODO
			}
		};
	}

	public ObservableValue<String> expectedNumberOfProp(OrdProduct forProduct) {
		return new SimpleStringProperty(
				forProduct.getExpectedNumberOf() != null ? forProduct.getExpectedNumberOf().toString() : null) {
			@Override
			public void set(String newValue) {
				forProduct.setExpectedNumberOf(Double.valueOf(newValue));
				orderRepository.updateOrdProduct(forProduct);
			}
		};
	}

	public ObservableValue<String> actualNumberOfProp(OrdProduct forProduct) {
		return new SimpleStringProperty(
				forProduct.getActualNumberOf() != null ? forProduct.getActualNumberOf().toString() : null) {
			@Override
			public void set(String newValue) {
				forProduct.setActualNumberOf(Double.valueOf(newValue));
				orderRepository.updateOrdProduct(forProduct);
			}
		};
	}

	public ObservableValue<String> ordProductCommentProp(OrdProduct forProduct) {
		return new SimpleStringProperty(forProduct.getComment()) {
			@Override
			public void set(String newValue) {
				forProduct.setComment(newValue);
				orderRepository.updateOrdProduct(forProduct);
			}
		};
	}

	public OrdProduct deleteOrdProduct(Object key) throws NotFoundException {
		return orderRepository.deleteOrdProduct(key);
	}

	public boolean updateOrder(Order order) {
		return orderRepository.update(order);
	}

	public boolean updateOrdProduct(OrdProduct product) {
		return orderRepository.updateOrdProduct(product);
	}

	public List<Product> availableProducts() {
		return productRepository.getAll();
	}

	public Product getProduct(String name) throws NotFoundException {
		return productRepository.get(name);
	}

	public boolean addOrdProduct(OrdProduct product) {
		return orderRepository.addOrdProduct(product);
	}

	public boolean addOrder(Order order) {
		return orderRepository.add(order);
	}

}
