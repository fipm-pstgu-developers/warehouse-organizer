package org.pstgu.WarehouseOrganizer.JavaFXBinder;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.pstgu.WarehouseOrganizer.InfoModel.MenuRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.NotFoundException;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Menu.FoodReception;

public class MenuRepositoryBinder {
	private Logger log = Logger.getLogger(getClass().getName());
	MenuRepository menuRepository;

	public MenuRepositoryBinder(MenuRepository menuRepository) {
		this.menuRepository = menuRepository;
	}
	
	public Map<FoodReception, List<Dish>> getMenuOnDay(LocalDate day) throws NotFoundException{
		return menuRepository.get(day).getDishes();
	}

}
