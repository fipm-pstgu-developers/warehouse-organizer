package org.pstgu.WarehouseOrganizer.JavaFXBinder;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.pstgu.WarehouseOrganizer.InfoModel.FoodRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.NotFoundException;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.FoodProduct;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class FoodRepositoryBinder {
	private Logger log = Logger.getLogger(getClass().getName());
	private ObservableList<FoodProduct> listOfFoodProduct;
	FoodRepository foodRepository;

	public FoodRepositoryBinder(FoodRepository foodRepository) {
		this.foodRepository = foodRepository;
	}

	public ObservableList<FoodProduct> getListOfFoodProduct() {
		listOfFoodProduct = FXCollections.observableList(foodRepository.getAll());
//		listOfFoodProduct.addListener(new ListChangeListener<FoodProduct>() {
//
//			@Override
//			public void onChanged(Change<? extends FoodProduct> c) {
//				while (c.next()) {
//					if (c.wasAdded()) {
//						for (FoodProduct p : c.getAddedSubList()) {
//							foodRepository.add(p);
//						}
//					} else if (c.wasRemoved()) {
//						for (FoodProduct p : c.getRemoved()) {
//							try {
//								foodRepository.delete(p);
//							} catch (NotFoundException e) {
//								log.log(Level.SEVERE, e.getMessage());
//							}
//						}
//					}
//
//				}
//			}
//		});
		return listOfFoodProduct;
	}

	public ObservableValue<String> nameProperty(FoodProduct forProduct) {
		return new SimpleStringProperty(forProduct.getProduct().getName()) {
			@Override
			public void set(String newValue) {
				forProduct.getProduct().setName(newValue);
				foodRepository.update(forProduct);
			}
		};
	}

	public ObservableValue<String> numberOfProperty(FoodProduct forFoodProduct) {
		return new SimpleStringProperty(forFoodProduct.getNumberOf().toString()) {
			@Override
			public void set(String newValue) {
				forFoodProduct.setNumberOf(Double.parseDouble(newValue));
				foodRepository.update(forFoodProduct);
			}
		};
	}

	public ObservableValue<String> measureUnitProperty(FoodProduct forProduct) {
		return new SimpleStringProperty(forProduct.getProduct().getMeasureUnit()) {
			@Override
			public void set(String newValue) {
				forProduct.getProduct().setMeasureUnit(newValue);
				foodRepository.update(forProduct);
			}
		};
	}

	public ObservableValue<String> locationProperty(FoodProduct forFoodProduct) {
		return new SimpleStringProperty(forFoodProduct.getLocation()) {
			@Override
			public void set(String newValue) {
				forFoodProduct.setLocation(newValue);
				foodRepository.update(forFoodProduct);
			}
		};
	}

//	public ObjectProperty<Product> productProperty(Product forProduct) throws NotFoundException, SQLException {
//		return new SimpleObjectProperty<Product>(myDB.getProduct(forProduct.getName())) {
//			@Override
//			public void set(Product newValue) {
//				Product product = p.getValue();
//
//				productsDB.updateProduct(product);
//			}
//		};
//	}

}
