package org.pstgu.WarehouseOrganizer.Application;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pstgu.WarehouseOrganizer.InfoModel.DishRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.FoodRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.MenuRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.NotFoundException;
import org.pstgu.WarehouseOrganizer.InfoModel.OrderRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.ProductRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.RepositoryManager;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Dish;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.DishIngredient;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.FoodProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.GiveOutTask;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Menu;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Task;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Menu.FoodReception;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Task.TaskType;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Controller {
	private static final String BREAD = "леб";

	private Logger log = Logger.getLogger(getClass().getName());

	private ObservableList<Task> tasks;
	private ObservableList<Task> completedTasks;
	private RepositoryManager repositoryManager;

	public Controller(RepositoryManager repositoryManager) {
		this.repositoryManager = repositoryManager;

		createOrders();

		tasks = FXCollections.observableArrayList();
		completedTasks = FXCollections.observableArrayList();

		boolean completed = getOrderRepository().getUnsentBreadOrders().isEmpty();
		tasks.add(new Task("Купить хлеб", "Необходимо заказать хлеб", TaskType.BREAD_ORDERS, completed) {
			@Override
			public void run() {
				moveToCompletedTasks(this);
			}
		});

		createGiveOutTask("Выдать завтрак", "Выдать завтрак дежурному", FoodReception.FIRST);
		createGiveOutTask("Выдать обед", "Выдать обед дежурному", FoodReception.SECOND);
		createGiveOutTask("Выдать ужин", "Выдать ужин дежурному", FoodReception.FOURTH);
		createGiveOutTask("Выдать предотбойник", "Выдать предотбойник дежурному", FoodReception.THIRD);

		tasks.add(new Task("Заказать продукты", "Сегодня необходимо заказать продукты", TaskType.DAILY_ORDERS,
				getOrderRepository().getUnsentNotBreadOrders().isEmpty()) {
			@Override
			public void run() {
				moveToCompletedTasks(this);
			}
		});

	}

	private void createGiveOutTask(String name, String summary, FoodReception foodReception) {
		try {
			List<Dish> dishes = getMenuRepository().get(LocalDate.now()).getDishes().get(foodReception);
			boolean completed = dishes.isEmpty()
					|| dishes.stream().map(dish -> dish.isGaveOut()).reduce((e1, e2) -> e1 && e2).orElse(true);
			tasks.add(new GiveOutTask(name, summary, foodReception, completed) {
				@Override
				public void run() {
					try {
						Menu menu = getMenuRepository().get(LocalDate.now());
						give(getFoodReception(), menu);
						moveToCompletedTasks(this);
					} catch (NotFoundException e) {
						e.printStackTrace();
					}
				}
			});
		} catch (NotFoundException e) {
			e.printStackTrace();
			log.log(Level.INFO, e.getMessage());
		}
	}

	/**
	 * Выдать продукты дежурному
	 *
	 * @param type - завтрак, обед, ужин или предотбойник
	 * @param menu - таблица с блюдами, как в жизни
	 */
	void give(Menu.FoodReception type, Menu menu) {
		for (Dish dish : menu.getDishes().get(type)) {
			dish.setGivenOut(true);
			for (DishIngredient ingredient : dish.getIngredients()) {
				double numberOf = ingredient.getActualNumberOf() != null ? ingredient.getActualNumberOf()
						: ingredient.getExpectedNumberOf();
				FoodProduct foodProduct = new FoodProduct(ingredient.getProduct(), numberOf, null);
				writeOffFromWarehouse(foodProduct);
			}
		}
		getMenuRepository().update(menu);
	}

	/**
	 * Принять заказ от поставщика и поместить продукты на склад (Добавить продукты
	 * на склад).
	 *
	 * @param order - сам заказ с указанным актуальным количеством продуктов
	 */
	public void accept(Order order) {

		order.setReceiveDate(LocalDate.now());
		getOrderRepository().update(order);

		FoodRepository foodRepository = getFoodRepository();

		for (OrdProduct ordProduct : order.getProducts()) {
			Product product = ordProduct.getProduct();
			FoodProduct fProduct = new FoodProduct(product,
					ordProduct.getActualNumberOf() > 0.0 ? ordProduct.getActualNumberOf()
							: ordProduct.getExpectedNumberOf(),
					null);
			addToWarehouse(fProduct);
		}

	}

	/**
	 * Сохранить личный заказ кладовщика
	 *
	 * @param order
	 */
	void create(Order order) {
		getOrderRepository().add(order);
	}

	/**
	 * Создает заказы на основе меню и запасов продуктов на складе
	 *
	 * @return
	 */
	void createOrders() {
		OrderRepository orders = getOrderRepository();

		orders.addAll(createBreadOrdersTo(LocalDate.now().plusDays(1)));

		orders.addAll(createOrdersTo(LocalDate.now().plusDays(2)));
	}

	private List<Order> createOrdersTo(LocalDate date) {
		List<Order> orders = new ArrayList<>();

		for (Provider provider : getOrderRepository().getProvidersIn(date)) {
			List<OrdProduct> productsToOrder = new ArrayList<>();
			Map<Product, Double> productsForPeriod = new HashMap<>();

			LocalDate endDate = provider.nextArrivalDateAfter(date).toLocalDate().plusDays(1);
			for (LocalDate dateI = date; dateI.isBefore(endDate); dateI = dateI.plusDays(1)) {
				Set<DishIngredient> ingredientsForDay = getMenuRepository().getAllProductsFromMenuFor(dateI);
				Map<Product, Double> productsForDay = ingredientsForDay.parallelStream().collect(Collectors
						.toMap(DishIngredient::getProduct, DishIngredient::getExpectedNumberOf, (x1, x2) -> x1));
				// merge tables
				productsForPeriod = Stream
						.concat(productsForDay.entrySet().stream(), productsForPeriod.entrySet().stream())
						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1 + v2));

			}
			for (Product product : provider.getSuppliedProducts()) {
				if (!product.getName().toLowerCase().contains(BREAD) && productsForPeriod.keySet().contains(product)) {
					double numberOfInWarehouse = 0;
					try {
						numberOfInWarehouse = getFoodRepository().get(product).getNumberOf();
					} catch (NotFoundException e) {
					}
					double orderedNumberOf = getOrderedNumberOf(product);

					double expectedNumberOf = numberOfInWarehouse + orderedNumberOf - productsForPeriod.get(product);
					if (expectedNumberOf < product.getEmergencyBalance()) {
						productsToOrder
								.add(new OrdProduct(product, -expectedNumberOf + product.getEmergencyBalance(), ""));
					}
				}
			}

			if (!productsToOrder.isEmpty()) {
				orders.add(new Order(LocalDate.now(), date, provider, "", productsToOrder));
			}
		}
		return orders;
	}

	private double getOrderedNumberOf(Product product) {
		return getOrderRepository().getOrdPoductsFromUnsentOrders().stream()
				.map(ordProd -> ordProd.getProduct().equals(product) ? ordProd.getExpectedNumberOf() : 0)
				.reduce((a, b) -> a + b).orElse(0.0);
	}

	private List<Order> createBreadOrdersTo(LocalDate date) {
		List<Order> orders = new ArrayList<>();

		List<Product> bread = getProductRepository().search(BREAD);

		Set<DishIngredient> productsForDay = getMenuRepository().getAllProductsFromMenuFor(date);
		for (Provider provider : getOrderRepository().getAllProviders()) {
			List<OrdProduct> productsToOrder = new ArrayList<>();
			for (Product breadP : bread) {
				if (provider.getSuppliedProducts().contains(breadP)) {
					for (DishIngredient ingredient : productsForDay) {
						if (ingredient.getProduct().equals(breadP)) {
							double numberOfInWarehouse = 0;
							try {
								numberOfInWarehouse = getFoodRepository().get(breadP).getNumberOf();
							} catch (NotFoundException e) {
							}
							double orderedNumberOf = getOrderedNumberOf(breadP);

							double expectedNumberOf = numberOfInWarehouse + orderedNumberOf
									- ingredient.getExpectedNumberOf();
							if (expectedNumberOf < breadP.getEmergencyBalance()) {
								productsToOrder.add(
										new OrdProduct(breadP, -expectedNumberOf + breadP.getEmergencyBalance(), ""));
							}
						}
					}
				}
			}
			if (!productsToOrder.isEmpty()) {
				orders.add(new Order(LocalDate.now(), date, provider, "", productsToOrder));
			}
		}
		return orders;
	}

	void execute(Task task) {
		task.run();
		tasks.remove(tasks.indexOf(task));
		completedTasks.add(task);
	}

	private void addToWarehouse(FoodProduct fProduct) {
		try {
			FoodProduct pInWarehouse = getFoodRepository().get(fProduct.getProduct().getName());
			pInWarehouse.setNumberOf(pInWarehouse.getNumberOf() + fProduct.getNumberOf());
			getFoodRepository().update(pInWarehouse);
		} catch (NotFoundException e) {
			log.log(Level.WARNING, e.getMessage());
			getFoodRepository().add(fProduct);
		}
	}

	private boolean writeOffFromWarehouse(FoodProduct product) {
		try {
			FoodProduct foodProduct = getFoodRepository().get(product.getProduct());
			double numberOf = foodProduct.getNumberOf() - product.getNumberOf();
			if (numberOf < 0) {
				throw new Exception("There is not enough " + foodProduct + " in warehouse to give out.");
			}
			foodProduct.setNumberOf(numberOf);
			getFoodRepository().update(foodProduct);

			log.log(Level.INFO, foodProduct + " were given out.");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.WARNING, e.getMessage());
			return false;
		}

	}

	private void moveToCompletedTasks(Task task) {
		task.setCompleted(true);
		if (tasks.remove(task))
			completedTasks.add(0, task);
	}

	private MenuRepository getMenuRepository() {
		return repositoryManager.getMenuRepository();
	}

	private OrderRepository getOrderRepository() {
		return repositoryManager.getOrderRepository();
	}

	private DishRepository getDishRepository() {
		return repositoryManager.getDishRepository();
	}

	private ProductRepository getProductRepository() {
		return repositoryManager.getProductRepository();
	}

	private FoodRepository getFoodRepository() {
		return repositoryManager.getFoodRepository();
	}

	public ObservableList<Task> getTasks() {
		return tasks;
	}

	public ObservableList<Task> getCompletedTasks() {
		return completedTasks;
	}

}
