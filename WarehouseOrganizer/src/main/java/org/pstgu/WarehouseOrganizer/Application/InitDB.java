package org.pstgu.WarehouseOrganizer.Application;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InitDB {
	private static Logger log = Logger.getLogger(InitDB.class.getName());

	public static void main(String[] args) {
		Connection connection = null;
		try {
			// db parameters
			final String url = "jdbc:sqlite:src/main/resources/db.sqlite";
			// create a connection to the database
			connection = DriverManager.getConnection(url);

			final String path = "src/main/resources/db.sql";
			String sql = new String(Files.readAllBytes(Paths.get(path)));
			if (connection.createStatement().executeUpdate(sql) != 0) {
				throw new SQLException("Execution failed for the command:\n\n" + sql);
			}

			log.log(Level.INFO, "Connection to SQLite has been established.");

		} catch (SQLException | IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		} finally {
			try {
				if (connection != null) {
					connection.close();
					log.log(Level.INFO, "Connection to SQLite has been closed.");
				}
			} catch (SQLException e) {
				log.log(Level.SEVERE, e.getMessage());
			}
		}
	}

}
