package org.pstgu.WarehouseOrganizer.Application;

import org.pstgu.WarehouseOrganizer.InfoModel.RepositoryManagerImpl;
import org.pstgu.WarehouseOrganizer.LayoutBuilder.ContentPane;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

	private final String DEFAULT_STYLESHEETS = "application.css";
	private static RepositoryManagerImpl repositoryManager;

	@Override
	public void start(Stage primaryStage) {
		try {
			Controller controller = new Controller(repositoryManager);

			Pane root = new ContentPane(controller, repositoryManager);

			Scene scene = new Scene(root, 800, 600);

			scene.getStylesheets().add(getClass().getResource(DEFAULT_STYLESHEETS).toExternalForm());
			primaryStage.setScene(scene);
			//primaryStage.setMaximized(true);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		repositoryManager = new RepositoryManagerImpl("jdbc:sqlite:src/main/resources/db.sqlite");
		launch(args);
		repositoryManager.closeDBConnection();
	}
}
