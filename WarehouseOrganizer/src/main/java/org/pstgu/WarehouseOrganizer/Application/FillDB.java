package org.pstgu.WarehouseOrganizer.Application;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.pstgu.WarehouseOrganizer.InfoModel.OrderRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.ProductRepository;
import org.pstgu.WarehouseOrganizer.InfoModel.RepositoryManagerImpl;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.OrdProduct;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Order;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Product;
import org.pstgu.WarehouseOrganizer.InfoModel.Data.Provider;

public class FillDB {
	private static RepositoryManagerImpl repositoryManager;

	public static void main(String[] args) {
		repositoryManager = new RepositoryManagerImpl("jdbc:sqlite:src/main/resources/db.sqlite");

		addProducts();
		addOrders();

		repositoryManager.closeDBConnection();
	}

	static void addProducts() {
		ProductRepository productRepository = repositoryManager.getProductRepository();
		Product[] products = { new Product("Картофель", "кг"), new Product("Сгущенка", "банка"),
				new Product("Скумбрия", "консерва"), new Product("Сахар", "кг"),
				new Product("Грецкие орехи", "пачка 300г"), new Product("Чай", "пачка 100 г"),
				new Product("Соль", "пачка 1,5кг"), new Product("Капуста", "головка"), new Product("Яблоки", "шт"),
				new Product("Кукуруза", "банка") };

		for (Product product : products) {
			productRepository.add(product);
//			System.out.println("productFillDB: " + product);
		}
	}

	static void addOrders() {
		OrderRepository orderRepository = repositoryManager.getOrderRepository();

		List<OrdProduct> productList = Arrays
				.asList(new OrdProduct(new Product("Греча", "Пакет"), 50.0, "Греча всегда кстати"));
		List<OrdProduct> productList1 = Arrays
				.asList(new OrdProduct(new Product("Рис", "Пакет"), 50.0, "Рис всегда хорош"));
		List<OrdProduct> productList2 = Arrays
				.asList(new OrdProduct(new Product("Печенье", "Пачка"), 10.0, "Добавь печенья и будет хорошо"));
		List<OrdProduct> productList3 = Arrays.asList(new OrdProduct(new Product("Рыба", "Штука"), 5.0, " "));

		Provider provider = new Provider(7, "Бакалея", "79685647968", null, null); // TODO @theonefeo заменить null на
																					// значения
		Order[] orders = { new Order(LocalDate.parse("2021-02-10"), provider, "Заказ успешно доставлен", productList),
				new Order(LocalDate.parse("2021-02-11"), provider, "Заказ НЕ доставлен", productList1),
				new Order(LocalDate.parse("2021-02-12"), provider, "Заказ в пути", productList2),
				new Order(LocalDate.parse("2021-02-13"), provider, "Доставка уточняется", productList3) };

		for (Order order : orders) {
			orderRepository.add(order);
			System.out.println("orderFillDB: " + order);
		}

	}
}
