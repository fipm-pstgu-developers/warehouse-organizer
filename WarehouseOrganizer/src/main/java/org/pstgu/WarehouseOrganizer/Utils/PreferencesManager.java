package org.pstgu.WarehouseOrganizer.Utils;

import java.util.prefs.Preferences;

public class PreferencesManager {
	private static PreferencesManager instance;

	public static String NUM_OF_CHILDREN = "numberOfChildren";
	public static String NUM_OF_ADULTS = "numberOfAdults";
	public static int DEFAULT_NUM_OF = 100;

	public static double ADULT_SERVING_SIZE = 1.5;
	public static double CHILDS_SERVING_SIZE = 1.0;

	private Preferences userPrefs = Preferences.userNodeForPackage(PreferencesManager.class);

	public PreferencesManager() {
		if (instance == null)
			instance = this;
	}

	public static PreferencesManager getInstance() {
		return instance;
	}

	public String get(String key, String def) {
		return userPrefs.get(key, def);
	}

	public int get(String key, int def) {
		return userPrefs.getInt(key, def);
	}

	public void put(String key, String value) {
		userPrefs.put(key, value);
	}

	public void put(String key, int value) {
		userPrefs.putInt(key, value);
	}

	public double getQuantityMultiplier() {
		return ADULT_SERVING_SIZE * get(NUM_OF_ADULTS, DEFAULT_NUM_OF)
				+ CHILDS_SERVING_SIZE * get(NUM_OF_CHILDREN, DEFAULT_NUM_OF);
	}
}
