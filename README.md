# Организатор склада
Приложение для организации работы склада. Полноэкранное приложение на JavaFX.

# Запуск в режиме разработки
1. Установите в систему [JDK](https://jdk.java.net/) [11+](https://jdk.java.net/java-se-ri/11) (Java 11 и выше, на которой будет работать Eclipse) и JDK 8 Oracle ([1](https://cloud.mail.ru/public/kKZm/7KhxVsjjd), [2](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)) (Java 8, на которой будет работать наше приложение).
2. Скачайте [Eclipse IDE for Java Developers](https://www.eclipse.org/downloads/packages/), разархивируйте и запустите файл eclipse/eclipse.exe
3. Установите дополнение e(fx)clipse для подключения GUI-библиотеки JavaFX *(Help->Eclipse Marketpace->Find->"JavaFX"->e(fx)clipse->Install->"I accept..."->Finish->после установки закрыть Eclipse)* (загрузка и установка дополнения - 1-3мин., в это время можно выполнить следующий пункт)
4. Загрузите GIT-проект warehouse-organizer и поместите в каталог eclipse-workspace
5. ~~Подключите библиотеку Lombok в Eclipse:~~
    1. ~~запустите программу warehouse-organizer/WarehouseOrganizer/lib/lombok.jar на Java 8 *(командой `java -jar path/to/lombok.jar`)*~~
    2. ~~выберите путь к среде Eclipse *(Specify location)*~~
    3. ~~нажмите Install/Update и после подтвеждения настройки закройте программу.~~
6. Запустите Eclipse и импортируйте проект WarehouseOrganizer, который вложен в warehouse-organizer *(File->Import->General->"Existing projects..."->Next->Browse->path/to/WarehouseOrganizer->Open->Finish)*. Проект появится в вкладке Package Explorer
7. Настройте для проекта Java 8 *(Вкладка Package Explorer->нажмите на WarehouseOrganizer правой кнопкой мыши/Alt+Enter->Properties->Java Build Path->Libraries->JRE System Library->Edit->Installed JREs->Add->Standard VM->Next->выбрать JRE home (path/to/jdk1.8.0_202)->Finish->Apply and Close->Alternate JRE->jdk1.8.0_202->Finish->Apply, окно не закрывайте). Настройте компиллятор (Java Compiler->"Enable project..."->"Compiler compliance level"->1.8->Apply and Close)*. Проект готов к запуску
8. **Имеется несколько целей запуска WarehouseOrganizer.** 
    1. Для инициализации БД запустите класс org.pstgu.WarehouseOrganizer.Application.InitDB (Run->Run As->"InitDB - ..."->Ok). В консоли должны появиться сообщения 
	"...
	INFO: Connection to SQLite has been established.
	...
	INFO: Connection to SQLite has been closed."
    2. Для заполнения БД тестовыми значениями запустите класс org.pstgu.WarehouseOrganizer.Application.FillDB аналогично.
    3. Запуск основного приложения производится запуском org.pstgu.WarehouseOrganizer.Application.Main

# Запуск с помощью Gradle
1. Установите в систему [JDK](https://jdk.java.net/) [11+](https://jdk.java.net/java-se-ri/11) (Java 11 и выше)
2. Установите систему автоматической сборки Gradle 7+
3. Загрузите GIT-проект warehouse-organizer и перейдите в каталог WarehouseOrganizer
4. Выполните команду `gradle createDemoDB run` - для иницализации, заполнения БД тестовыми данными и запуска приложения
    1. `gradle initDB` - только для иницализации БД
    2. `gradle run` - только для запуска приложения

### P.S.
+ Версию Java, установленную в системе можно проверить командой `java -version`
+ Java 8 - это Java версии 1.8
+ После инициализации БД в проекте появляется файл, который Eclipse может увидеть только после обновления проекта *(Refresh/F5)*
+ Список доступных для проекта Gradle-задач можно получить командой `gradle tasks`
+ В Windows команда `gradle` заменяется на `%gradle%`

### Обратная связь
E-mail: [lysenko765@ya.ru](mailto:lysenko765@ya.ru)

### Лицензия
GNU General Public License
